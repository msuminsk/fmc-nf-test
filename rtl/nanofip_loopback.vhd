library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pkg_nanofip.all;

entity nanofip_loopback is
    port(
        clk_i : in std_logic;
        rst_i : in std_logic;

        -- Control button
        button_i : in std_logic;

        -- Wishbone
        dat_i  : in std_logic_vector(7 downto 0);
        dat_o  : out std_logic_vector(7 downto 0);
        adr_o  : out unsigned(9 downto 0);
        stb_o  : out std_logic;
        wclk_o : out std_logic;
        we_o   : out std_logic;
        cyc_o  : out std_logic;
        ack_i  : in std_logic;
        rst_o  : out std_logic;

        -- FIP
        rstin_o    : out std_logic;
        --rston_i    : in std_logic;    -- TODO remove?
        nostat_o   : out std_logic;
        var3_acc_o : out std_logic;
        var3_rdy_i : in std_logic;
        var2_acc_o : out std_logic;
        var2_rdy_i : in std_logic;
        var1_acc_o : out std_logic;
        var1_rdy_i : in std_logic;
        p3_lgth_o  : out std_logic_vector(2 downto 0)

        -- TODO monitor cacer, pacer, tler, fcser
    );
end nanofip_loopback;

architecture rtl of nanofip_loopback is
    type state_t is (IDLE, READ_STATUS, READ_SIZE, VAR_READ, VAR_WRITE, WAIT_UPDATE);
    signal state : state_t;

    type ram_t is array (natural range <>) of std_logic_vector(7 downto 0);
    signal ram : ram_t(127 downto 0);

    signal var_rdy, var_acc : std_logic;
    signal var_size, adr_cnt : unsigned(6 downto 0);

    -- Wishbone base address for the selected variable
    signal data_addr, size_addr : unsigned(adr_o'range);
    signal data_addr_reg, size_addr_reg : unsigned(adr_o'range);

    signal button_degl : std_logic;
    signal rst_n       : std_logic;
begin

wclk_o <= clk_i;
rst_o <= rst_i;
rstin_o <= not rst_i;   -- rstin is active low
rst_n   <= not rst_i;

nostat_o <= '1';        -- disable status byte in the produced variable
p3_lgth_o <= "000";     -- produced variable size: 2 bytes

but_deglitcher: entity work.gc_glitch_filt
    generic map(g_len => 16)
    port map(
        clk_i   => clk_i,
        rst_n_i => rst_n,
        dat_i   => button_i,
        dat_o   => button_degl
    );

-- Button selects the copied variable (high state => VAR1, low state => VAR2)
var_rdy <= var1_rdy_i when button_degl = '1' else var2_rdy_i;
var1_acc_o <= var_acc when button_degl = '1' else '0';
var2_acc_o <= var_acc when button_degl = '0' else '0';
data_addr <= NF_VAR1_DATA_START when button_degl = '1' else NF_VAR2_DATA_START;
size_addr <= NF_VAR1_SIZE when button_degl = '1' else NF_VAR2_SIZE;

process(clk_i)
begin
    if rising_edge(clk_i) then
        stb_o <= '0';
        we_o <= '0';
        cyc_o <= '0';

        if rst_i = '1' then
            state <= IDLE;
            adr_cnt <= (others => '0');
            var_size <= (others => '0');

            dat_o <= (others => '0');
            adr_o <= (others => '0');

            var_acc <= '0';
            var3_acc_o <= '0';
        else

            case state is
                -- wait for the consumed variable to be ready
                when IDLE =>
                    if var_rdy = '1' then
                        var_acc <= '1';
                        data_addr_reg <= data_addr;
                        size_addr_reg <= size_addr;
                        state <= READ_SIZE;
                    end if;

                -- read the consumed variable size
                when READ_SIZE =>
                    adr_o <= size_addr_reg;
                    stb_o <= '1';
                    cyc_o <= '1';

                    if ack_i = '1' then
                        var_size <= unsigned(dat_i(6 downto 0)) - 2;
                        adr_cnt <= (others => '0');
                        stb_o <= '0';
                        cyc_o <= '0';

                        if var_rdy = '0' then
                            state <= IDLE;
                        else
                            state <= VAR_READ;
                        end if;
                    end if;

                -- read the status to decide whether the produced variable
                -- need to be updated
                -- TODO refreshment bit seems to be always set
                --when READ_STATUS =>
                --    adr_o <= NF_VAR1_BASE + var_size + 1;
                --    stb_o <= '1';
                --    cyc_o <= '1';

                --    if ack_i = '1' then
                --        adr_cnt <= (others => '0');
                --        stb_o <= '0';
                --        cyc_o <= '0';

                --        -- update the variable if needed (refreshment bit is set)
                --        if dat_i(0) = '1' then
                --            state <= VAR_READ;
                --        else
                --            state <= IDLE;
                --        end if;
                --    end if;

                -- copy the consumed variable from nanoFIP to the internal RAM
                when VAR_READ =>
                    adr_o <= data_addr_reg + adr_cnt;
                    stb_o <= '1';
                    cyc_o <= '1';

                    if ack_i = '1' then
                        ram(to_integer(adr_cnt)) <= dat_i;
                        stb_o <= '0';

                        if var_rdy = '0' then
                            state <= IDLE;
                            var_acc <='0';
                        elsif adr_cnt = var_size then
                            -- all data has been read
                            state <= VAR_WRITE;
                            adr_cnt <= (others => '0');
                            var_acc <= '0';
                            cyc_o <= '0';
                        else
                            adr_cnt <= adr_cnt + 1;
                        end if;
                    end if;

                -- copy the data to the produced variable on nanoFIP
                when VAR_WRITE =>
                    if var3_rdy_i = '1' then
                        var3_acc_o <= '1';
                        adr_o <= NF_VAR3_DATA_START + adr_cnt;
                        dat_o <= ram(to_integer(adr_cnt));
                        cyc_o <= '1';
                        we_o <= '1';
                        stb_o <= '1';

                        if ack_i = '1' then
                            stb_o <= '0';

                            if adr_cnt = var_size then
                                -- all data has been read
                                state <= WAIT_UPDATE;
                                var3_acc_o <= '0';
                                cyc_o <= '0';
                            else
                                adr_cnt <= adr_cnt + 1;
                            end if;
                        end if;
                    end if;

                -- wait for varx_rdy to go low, meaning it is updated
                when WAIT_UPDATE =>
                    if var_rdy = '0' then
                        state <= IDLE;
                    end if;

                when others =>
                    state <= IDLE;
            end case;
        end if;
    end if;
end process;

end rtl;
