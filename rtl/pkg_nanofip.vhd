library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg_nanofip is
    -- Wishbone memory map
    constant NF_TYPE_OFFSET : unsigned(9 downto 0)          := "0000000000";
    constant NF_SIZE_OFFSET : unsigned(9 downto 0)          := "0000000001";
    constant NF_DATA_START_OFFSET : unsigned(9 downto 0)    := "0000000010";
    constant NF_DATA_END_OFFSET : unsigned(9 downto 0)      := "0001111111";

    constant NF_VAR1_BASE : unsigned(9 downto 0)        := "0000000000";
    constant NF_VAR1_TYPE : unsigned(9 downto 0)        := NF_VAR1_BASE + NF_TYPE_OFFSET;
    constant NF_VAR1_SIZE : unsigned(9 downto 0)        := NF_VAR1_BASE + NF_SIZE_OFFSET;
    constant NF_VAR1_DATA_START : unsigned(9 downto 0)  := NF_VAR1_BASE + NF_DATA_START_OFFSET;
    constant NF_VAR1_DATA_END : unsigned(9 downto 0)    := NF_VAR1_BASE + NF_DATA_END_OFFSET;


    constant NF_VAR2_BASE : unsigned(9 downto 0)        := "0010000000";
    constant NF_VAR2_TYPE : unsigned(9 downto 0)        := NF_VAR2_BASE + NF_TYPE_OFFSET;
    constant NF_VAR2_SIZE : unsigned(9 downto 0)        := NF_VAR2_BASE + NF_SIZE_OFFSET;
    constant NF_VAR2_DATA_START : unsigned(9 downto 0)  := NF_VAR2_BASE + NF_DATA_START_OFFSET;
    constant NF_VAR2_DATA_END : unsigned(9 downto 0)    := NF_VAR2_BASE + NF_DATA_END_OFFSET;

    constant NF_VAR3_BASE : unsigned(9 downto 0)        := "0100000000";
    constant NF_VAR3_DATA_START : unsigned(9 downto 0)  := "0100000010";
    constant NF_VAR3_DATA_END : unsigned(9 downto 0)    := "0101111101";
end package;
