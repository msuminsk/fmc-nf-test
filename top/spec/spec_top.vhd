library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;


entity spec_top is
  port(
    clk_125m_pllref_n_i : in std_logic;
    clk_125m_pllref_p_i : in std_logic;

    button1_i   : in std_logic;
    --button2_i   : in std_logic;
    led_red_o   : out std_logic;
    led_green_o : out std_logic;

    -- Wishbone
    dat_i  : in std_logic_vector(7 downto 0);
    dat_o  : out std_logic_vector(7 downto 0);
    adr_o  : out unsigned(9 downto 0);
    stb_o  : out std_logic;
    wclk_o : out std_logic;
    we_o   : out std_logic;
    cyc_o  : out std_logic;
    ack_i  : in std_logic;
    rst_o  : out std_logic;

    -- FIP
    rstin_o    : out std_logic;
    --rston_i    : in std_logic;
    nostat_o   : out std_logic;
    var3_acc_o : out std_logic;
    var3_rdy_i : in std_logic;
    var2_acc_o : out std_logic;
    var2_rdy_i : in std_logic;
    var1_acc_o : out std_logic;
    var1_rdy_i : in std_logic;
    p3_lgth_o  : out std_logic_vector(2 downto 0)
  );
end spec_top;

architecture rtl of spec_top is
  signal clk_40m, clk_40m_n, rst, locked : std_logic;
  signal rst_cnt : unsigned(5 downto 0) := (others => '0');

  component ODDR2 is
  generic(
      DDR_ALIGNMENT : string := "NONE";
      INIT          : bit    := '0';
      SRTYPE        : string := "SYNC"
  );
  port(
      Q           : out std_ulogic;

      C0          : in  std_ulogic;
      C1          : in  std_ulogic;
      CE          : in  std_ulogic := 'H';
      D0          : in  std_ulogic;
      D1          : in  std_ulogic;
      R           : in  std_ulogic := 'L';
      S           : in  std_ulogic := 'L'
  );
  end component ODDR2;

begin

led_green_o <= '0';
led_red_o <= '1';


-- power-on reset
process(clk_40m)
begin
  if rising_edge(clk_40m) then
    if rst_cnt /= (rst_cnt'range => '1') then
      rst_cnt <= rst_cnt + 1;
    end if;
  end if;
end process;

rst <= '0' when rst_cnt = (rst_cnt'range => '1') or locked = '0' else '1';


-- clocks
clk_generator: entity work.clk_gen
  port map (
    clk_125m_i_P => clk_125m_pllref_p_i,
    clk_125m_i_N => clk_125m_pllref_n_i,
    clk_40m_o    => clk_40m,
    clk_40m_n_o  => clk_40m_n,
    RESET        => '0',
    LOCKED       => locked
  );

clk_40m_oddr: ODDR2
  --generic map(
      --DDR_ALIGNMENT : string := "NONE";
      --INIT          : bit    := '0';
      --SRTYPE        : string := "SYNC"
  --)
  port map (
      Q       => wclk_o,
      C0      => clk_40m,
      C1      => clk_40m_n,
      CE      => '1',
      D0      => '1',
      D1      => '0',
      R       => '0',
      S       => '0'
  );


nf_loopback: entity work.nanofip_loopback
  port map (
    clk_i       => clk_40m,
    rst_i       => rst,
    button_i    => button1_i,

    dat_i       => dat_i,
    dat_o       => dat_o,
    adr_o       => adr_o,
    stb_o       => stb_o,
    --wclk_o      => wclk_o,    -- delivered by clk_40m_oddr
    we_o        => we_o,
    cyc_o       => cyc_o,
    ack_i       => ack_i,
    rst_o       => rst_o,

    rstin_o     => rstin_o,
    --rston_i     => rston_i,
    nostat_o    => nostat_o,
    var3_acc_o  => var3_acc_o,
    var3_rdy_i  => var3_rdy_i,
    var2_acc_o  => var2_acc_o,
    var2_rdy_i  => var2_rdy_i,
    var1_acc_o  => var1_acc_o,
    var1_rdy_i  => var1_rdy_i,
    p3_lgth_o   => p3_lgth_o
  );

end rtl;
