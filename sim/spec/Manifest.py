action = "simulation"
sim_tool = "modelsim"
sim_top = "main"
top_module = "main"

vlog_opt = "\+incdir\+../include \+incdir\+.. \+incdir\+../include/wb"
vcom_opt = "-2008 -mixedsvvh l"


# create a fake proasic3 library needed by nanofip
sim_pre_cmd = "vlib ../fake_proasic3; vmap proasic3 ../fake_proasic3"
#sim_post_cmd = "vsim -i work.main -do run.do"

include_dirs = [
    "../include",
    "../include/wb",
]

modules = {
    "local" : [
        "../../syn/spec",
        "../nanofip-gateware/top"
    ],
}

files = [
    "../nf_test_top.svh",
    "../ram4k9.v",      # ram4k9 model from Proasic3 library
]
