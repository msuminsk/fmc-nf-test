library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity spec_sim is
end spec_sim;

architecture Behavioral of spec_sim is
  signal led_red, led_grn : std_logic;
  signal wb_dat_o, wb_dat_i : unsigned(7 downto 0);
  signal wb_adr : unsigned(9 downto 0);
  signal wb_stb, wb_wclk, wb_we, wb_cyc, wb_ack, wb_rst : std_logic;
  signal rstin, rston, nostat, var3_acc, var3_rdy, var2_acc, var2_rdy, var1_acc, var1_rdy : std_logic;
  signal p3_lgth : unsigned(2 downto 0);

begin
  dut: entity work.spec_top
  port map(
    clk_125m_pllref_n_i => clk_125_n,
    clk_125m_pllref_p_i => clk_125_p,

    button1_i => '0',
    button2_i => '0',
    led_red   => led_red,
    led_green => led_grn,

    -- Wishbone
    dat_o     => wb_dat_o,
    dat_i     => wb_dat_i,
    adr_i     => wb_adr,
    stb_i     => wb_stb,
    wclk_i    => wb_wclk,
    we_i      => wb_we,
    cyc_i     => wb_cyc,
    ack_o     => wb_ack,
    rst_i     => wb_rst,

    -- FIP
    rstin     => rstin,
    rston     => rston,
    nostat    => nostat,
    var3_acc  => var3_acc,
    var3_rdy  => var3_rdy,
    var2_acc  => var2_acc,
    var2_rdy  => var2_rdy,
    var1_acc  => var1_acc,
    var1_rdy  => var1_rdy,
    p3_lgth   => p3_lgth
  );

end Behavioral;
