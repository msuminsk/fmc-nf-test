onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group Main -radix hexadecimal /main/clk_40m_nf
add wave -noupdate -group Main -radix hexadecimal /main/clk_125m_p
add wave -noupdate -group Main -radix hexadecimal /main/clk_125m_n
add wave -noupdate -group Main -radix hexadecimal /main/rst_n
add wave -noupdate -group Main -radix hexadecimal /main/dat_i
add wave -noupdate -group Main -radix hexadecimal /main/dat_o
add wave -noupdate -group Main -radix hexadecimal /main/adr
add wave -noupdate -group Main -radix hexadecimal /main/stb
add wave -noupdate -group Main -radix hexadecimal /main/wclk
add wave -noupdate -group Main -radix hexadecimal /main/we
add wave -noupdate -group Main -radix hexadecimal /main/cyc
add wave -noupdate -group Main -radix hexadecimal /main/ack
add wave -noupdate -group Main -radix hexadecimal /main/wrst
add wave -noupdate -group Main -radix hexadecimal /main/rstin
add wave -noupdate -group Main -radix hexadecimal /main/rston
add wave -noupdate -group Main -radix hexadecimal /main/var1_acc
add wave -noupdate -group Main -radix hexadecimal /main/var1_rdy
add wave -noupdate -group Main -radix hexadecimal /main/var2_acc
add wave -noupdate -group Main -radix hexadecimal /main/var2_rdy
add wave -noupdate -group Main -radix hexadecimal /main/var3_acc
add wave -noupdate -group Main -radix hexadecimal /main/var3_rdy
add wave -noupdate -group Main -radix hexadecimal /main/p3_lgth
add wave -noupdate -group Main -radix hexadecimal /main/nostat
add wave -noupdate -expand -group Fieldrive /main/fieldrive/fd_rxcdn
add wave -noupdate -expand -group Fieldrive /main/fieldrive/fd_rxd
add wave -noupdate -expand -group Fieldrive /main/fieldrive/fd_txer
add wave -noupdate -expand -group Fieldrive /main/fieldrive/fd_wdgn
add wave -noupdate -expand -group Fieldrive /main/fieldrive/fd_rstn
add wave -noupdate -expand -group Fieldrive /main/fieldrive/fd_txck
add wave -noupdate -expand -group Fieldrive /main/fieldrive/fd_txd
add wave -noupdate -expand -group Fieldrive /main/fieldrive/fd_txena
add wave -noupdate -expand -group Fieldrive /main/fieldrive/speed
add wave -noupdate -expand -group FipBus /main/fip_bus/p
add wave -noupdate -expand -group FipBus /main/fip_bus/n
add wave -noupdate -expand -group SPEC /main/SPEC/nf_loopback/state
add wave -noupdate -expand -group SPEC /main/SPEC/button1_i
add wave -noupdate -expand -group SPEC /main/SPEC/button2_i
add wave -noupdate -expand -group SPEC /main/SPEC/led_red_o
add wave -noupdate -expand -group SPEC /main/SPEC/led_green_o
add wave -noupdate -expand -group SPEC -radix binary /main/SPEC/adr_o
add wave -noupdate -expand -group SPEC -radix hexadecimal /main/SPEC/dat_i
add wave -noupdate -expand -group SPEC -radix hexadecimal /main/SPEC/dat_o
add wave -noupdate -expand -group SPEC /main/SPEC/stb_o
add wave -noupdate -expand -group SPEC /main/SPEC/wclk_o
add wave -noupdate -expand -group SPEC /main/SPEC/we_o
add wave -noupdate -expand -group SPEC /main/SPEC/cyc_o
add wave -noupdate -expand -group SPEC /main/SPEC/ack_i
add wave -noupdate -expand -group SPEC /main/SPEC/rst_o
add wave -noupdate -expand -group SPEC /main/SPEC/rstin_o
add wave -noupdate -expand -group SPEC /main/SPEC/rston_i
add wave -noupdate -expand -group SPEC /main/SPEC/nostat_o
add wave -noupdate -expand -group SPEC /main/SPEC/var3_acc_o
add wave -noupdate -expand -group SPEC /main/SPEC/var3_rdy_i
add wave -noupdate -expand -group SPEC /main/SPEC/var2_acc_o
add wave -noupdate -expand -group SPEC /main/SPEC/var2_rdy_i
add wave -noupdate -expand -group SPEC /main/SPEC/var1_acc_o
add wave -noupdate -expand -group SPEC /main/SPEC/var1_rdy_i
add wave -noupdate -expand -group SPEC /main/SPEC/p3_lgth_i
add wave -noupdate -expand -group SPEC /main/SPEC/clk_40m
add wave -noupdate -expand -group SPEC /main/SPEC/rst
add wave -noupdate -expand -group SPEC /main/SPEC/locked
add wave -noupdate -expand -group SPEC /main/SPEC/rst_cnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {230839983 ps} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {230016273 ps} {231672567 ps}
