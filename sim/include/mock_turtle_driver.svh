`ifndef __MOCK_TURTLE_DRIVER_INCLUDED
 `define __MOCK_TURTLE_DRIVER_INCLUDED

 `include "simdrv_defs.svh"
 `include "mt_cpu_csr_driver.svh"
 `include "mt_mqueue_host_driver.svh"

class MockTurtleDriver;
   protected CBusAccessor m_acc;
   protected uint64_t m_base;
   protected NodeCPUControl m_cpu_csr;
   protected MQueueHost m_hmq;

   function NodeCPUControl CPUs();
      return m_cpu_csr;
   endfunction // CPUs

   function MQueueHost HMQ();
      return m_hmq;
   endfunction // CPUs
   
   function  new ( CBusAccessor acc, uint64_t base );
      m_acc = acc;
      m_base = base;
   endfunction // new

   task automatic init();
      m_cpu_csr = new ( m_acc, m_base + 'hc000 );
      m_hmq = new ( m_acc, m_base + 'h0000 );
      m_cpu_csr.init();
      m_hmq.init();
   endtask // init

   // polls HMQ and Debug UART
   task automatic update();
      m_cpu_csr.update();
      m_hmq.update();
   endtask // update

endclass // MockTurtleDriver

`endif //  `ifndef __MOCK_TURTLE_DRIVER_INCLUDED
