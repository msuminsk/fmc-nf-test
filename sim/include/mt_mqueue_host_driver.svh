`ifndef __MT_MQUEUE_HOST_DRIVER_INCLUDED
 `define __MT_MQUEUE_HOST_DRIVER_INCLUDED

`include "serializable.svh"

`define MQUEUE_BASE_IN(x) ('h4000 + (x) * 'h400)
`define MQUEUE_BASE_OUT(x) ('h8000 + (x) * 'h400)

`define MQUEUE_CMD_CLAIM (1<<24)
`define MQUEUE_CMD_PURGE (1<<25)
`define MQUEUE_CMD_READY (1<<26)
`define MQUEUE_CMD_DISCARD (1<<27)

`define MQUEUE_SLOT_COMMAND 0
`define MQUEUE_SLOT_STATUS 4

`define MQUEUE_GCR_OUTGOING_STATUS_MASK (32'h0000ffff)

`define MQUEUE_GCR_SLOT_COUNT 0
`define MQUEUE_GCR_SLOT_STATUS 4
`define MQUEUE_GCR_IRQ_MASK 8
`define MQUEUE_GCR_IRQ_COALESCE 12


class MQueueMessage extends Serializable;

   uint32_t m_payload[$];
   
   
   function new ( uint32_t data[$] );
      m_payload = data;
   endfunction // new

   virtual function automatic void serialize( ByteBuffer data );
      for ( int i = 0; i < m_payload.size(); i++)
	data.addWord(m_payload[i]);
   endfunction // serialize
   

endclass // MQueueMessage


class MQueueCB;
   protected CBusAccessor bus;
   protected uint32_t base;
   
   function new ( CBusAccessor bus_, input uint32_t base_);
      base = base_;
      bus = bus_;
   endfunction // new

   task outgoing_write ( int slot, uint32_t r, uint32_t v );
      
      bus.write ( base + `MQUEUE_BASE_OUT(slot) + r, v);
   endtask // slot_write
   
   
   task send(int slot, uint32_t data[] );
      int i;
      
      outgoing_write( slot, `MQUEUE_SLOT_COMMAND, `MQUEUE_CMD_CLAIM);
      for(i=0;i<data.size(); i++)
        outgoing_write( slot, 8 + i * 4, data[i]);
      outgoing_write( slot, `MQUEUE_SLOT_COMMAND, `MQUEUE_CMD_READY | data.size());
   endtask // send
   
endclass 

class MQueueHost;
   protected CBusAccessor bus;
   protected uint32_t base;
   protected bit initialized;

   protected int n_in, n_out;

   typedef uint32_t word_array_t[$];
   
  
   typedef struct {
      uint32_t data[$];
   } mqueue_message_t;
     
   typedef mqueue_message_t slot_queue_t[$];
      
   slot_queue_t slots_in[16], slots_out[16];
   
   function bit poll(int slot);
      return slots_in[slot].size() != 0;
   endfunction // poll

   function mqueue_message_t recv (int slot);
      mqueue_message_t tmp = slots_in[slot][$];
      slots_in[slot].pop_back();
      return tmp;
   endfunction // recv

   function ByteBuffer recvBuf( int slot );

   endfunction // recvBuf


   function  word_array_t pack(ByteBuffer b);
      word_array_t msg;
      uint32_t pos = 0;
      uint32_t remaining;

      remaining = b.data.size();
      
      while(remaining > 0)      
	begin
	   uint32_t s = remaining > 4 ? 4 : remaining;
	   uint32_t word = 0;

	   remaining -= s;
	   
	   for (int i = 0; i < s; i++)
	     begin
		word |= b.data[pos] << ((3-i) * 8);
		pos++;
	     end

	   msg.push_back(word);
	end

      return msg;
      
   endfunction // pack

   function automatic ByteBuffer unpack (word_array_t data);
      ByteBuffer b = new;
      for(int i = 0; i < data.size(); i++)
	begin
	   b.data.push_back( (data[i] >> 24) & 'hff);
	   b.data.push_back( (data[i] >> 16) & 'hff);
	   b.data.push_back( (data[i] >> 8) & 'hff);
	   b.data.push_back( (data[i] >> 0) & 'hff);
	end
      return b;
      
   endfunction // unpack
   
   task sendMessage (int slot, Serializable data);
      mqueue_message_t msg;
      ByteBuffer b = new;

      data.serialize(b);
      msg.data = pack(b);
      
      incoming_send( slot, msg.data );
   endtask // send

   task sendWords (int slot, uint32_t data[$]);
      incoming_send( slot, data );
   endtask // send



   task automatic receiveMessage( int slot, Serializable data, ref int status,  input real timeout_us = 1000 );
      time t0;
      
      word_array_t msg_out;
      uint32_t out_stat;
      
      t0 = $time;
      
      while ( real'($time - t0) / real'(1us) < timeout_us )
	begin

	   gcr_read( `MQUEUE_GCR_SLOT_STATUS, out_stat);
 	   out_stat &= `MQUEUE_GCR_OUTGOING_STATUS_MASK;

	   if( out_stat & ( 1<< slot ) ) begin
	      
	      outgoing_input( slot, msg_out );
	      data.deserialize( unpack ( msg_out) );
	      status = 0;
	      
	      return;
	      
	   end
	   
	   
	   #100ns;
	end

//      $error("sendAndReceiveSync: timeout [%d us] expired!", int'(timeout_us));
      status = -1;
    
   endtask // receiveMessage
   
   
   task sendAndReceiveSync ( int slot_in, Serializable data_in, int slot_out, Serializable data_out, real timeout_us = 1000000, ref int status );
      time t0;
      
      ByteBuffer buf_in = new;
      word_array_t msg_in, msg_out;
      uint32_t out_stat;

      
      data_in.serialize(buf_in);
      msg_in = pack(buf_in);
      incoming_send( slot_in, msg_in );
      t0 = $time;
      
      while ( real'($time - t0) / real'(1us) < timeout_us )
	begin

	   gcr_read( `MQUEUE_GCR_SLOT_STATUS, out_stat);
 	   out_stat &= `MQUEUE_GCR_OUTGOING_STATUS_MASK;

	   if( out_stat & ( 1<< slot_out ) ) begin
	      
	      outgoing_input( slot_out, msg_out );
	      data_out.deserialize( unpack ( msg_out) );
	      status = 0;
	      
	      return;
	      
	   end
	   
	   
	   #100ns;
	end

      $error("sendAndReceiveSync: timeout [%d us] expired!", int'(timeout_us));
      status = -1;
      
   endtask // sendAndReceiveSync
   
	
   

   task send (int slot, uint32_t data[$]);
      mqueue_message_t msg;
      msg.data = data;
      slots_out[slot].push_back(msg);
   endtask // send

   function int idle();
      int i;
      for(i=0;i<n_out;i++)
        if(slots_out[i].size())
          return 0;
      return 1;
   endfunction // idle
   
        
      
     
   
   function new( CBusAccessor bus_, input uint32_t base_);
      base = base_;
      bus = bus_;
      initialized = 0;
      
   endfunction // new
   
   task incoming_write ( int slot, uint32_t r, uint32_t v );
      
      bus.write ( base + `MQUEUE_BASE_IN(slot) + r, v );
   endtask // slot_write

   task incoming_read ( int slot, uint32_t r, ref uint32_t v );
      uint64_t tmp;
      
      bus.read ( base + `MQUEUE_BASE_IN(slot) + r, tmp );
      v= tmp;
      
   endtask // slot_write

   task outgoing_read ( int slot, uint32_t r, ref uint32_t v );
      uint64_t tmp;
      
      bus.read ( base + `MQUEUE_BASE_OUT(slot) + r, tmp );
      v= tmp;
   endtask // slot_write

   task outgoing_write ( int slot, uint32_t r, uint32_t v );
      bus.write ( base + `MQUEUE_BASE_OUT(slot) + r, v);
   endtask // slot_write

   
   task gcr_read ( uint32_t r, output uint32_t rv );
      uint64_t tmp;
      
      bus.read ( base + r, tmp );
      rv = tmp;
      
   endtask // gcr_read

   task outgoing_check_full( int slot, output int full );
      uint32_t rv;
      outgoing_read( slot, `MQUEUE_SLOT_STATUS, rv);
      full = (rv & 1) ? 1:  0;
   endtask // outgoing_full


   task incoming_check_full( int slot, output int full );
      uint32_t rv;
      incoming_read( slot, `MQUEUE_SLOT_STATUS, rv);
      full = (rv & 1) ? 1:  0;
   endtask // outgoing_full


   task automatic incoming_send(int slot, uint32_t data[$] );
      int i;

      forever begin
	 automatic int full;
	 incoming_check_full(slot, full);
	 if(!full)
	   break;
      end
      
      incoming_write( slot, `MQUEUE_SLOT_COMMAND, `MQUEUE_CMD_CLAIM);
      for(i=0;i<data.size(); i++)
	begin
        incoming_write( slot, 8 + i * 4, data[i]);
	   
	   end
      incoming_write( slot, `MQUEUE_SLOT_COMMAND, `MQUEUE_CMD_READY | data.size());

   endtask // send

   task init();
      uint32_t slot_count, slot_status;
      int i, entries, size;
      
      gcr_read(`MQUEUE_GCR_SLOT_COUNT, slot_count);

      n_in = slot_count & 'hff;
      n_out = (slot_count >> 8) & 'hff;

//      $display("HMQ init: CPU->Host (outgoing) slots: %d Host->CPU (incoming) slots: %d", n_out, n_in);
      for(i =0 ; i<n_out; i++)begin
         outgoing_read(i, `MQUEUE_SLOT_STATUS, slot_status);
	 outgoing_write(i, `MQUEUE_SLOT_COMMAND, `MQUEUE_CMD_PURGE );
	 
         size = 1 << (( slot_status >> 28) & 'hf);
         entries = 1 << (( slot_status >> 2) & 'h3f);
         
  //       $display(" - out%d: size=%d, entries=%d", i, size, entries);

      end

      for(i =0 ; i<n_in; i++)
        begin
         incoming_read(i, `MQUEUE_SLOT_STATUS, slot_status);
	   incoming_write(i, `MQUEUE_SLOT_COMMAND, `MQUEUE_CMD_PURGE );
         size = 1 << (( slot_status >> 28) & 'hf);
         entries = 1 << (( slot_status >> 2) & 'h3f);
         
    //     $display(" - in%d: size=%d, entries=%d", i, size, entries);

      end
      
      initialized = 1;
      
   endtask // init

   task automatic outgoing_input(int slot, ref uint32_t data[$]);
      uint32_t stat;
      int count, i;

      outgoing_read ( slot, `MQUEUE_SLOT_STATUS, stat );

//      $display("Input[%d]: stat %x", slot, stat );
      
      count = (stat >> 16) & 'hff;

      for(i=0;i<count;i++)begin
         uint32_t d;
	 
         outgoing_read ( slot, 8 + i * 4, d );
	 data.push_back(d);
	 
      end

      outgoing_write( slot, `MQUEUE_SLOT_COMMAND, `MQUEUE_CMD_DISCARD );
      
   endtask // read_incoming
   
   task update();
      uint32_t in_stat, irq_mask;
      uint32_t rx_data[$];
      int i;
      
      
      if(!initialized)
        init();

      gcr_read( `MQUEUE_GCR_SLOT_STATUS, in_stat);
      gcr_read( `MQUEUE_GCR_IRQ_MASK, irq_mask);

      if(in_stat & `MQUEUE_GCR_OUTGOING_STATUS_MASK)
        begin
           for(i = 0; i < n_in ;i++)
             if(in_stat & (1<<i))
	       begin
                  outgoing_input (i, rx_data);
	       end
	   
           
        end
      
      for(i = 0; i < n_out ;i++)
        begin
           if ( slots_out[i].size() )
             begin
		
                mqueue_message_t msg = slots_out[i].pop_back();
		
		incoming_send( i, msg.data );

             end
        end
      
      
      
      
   endtask // update
   

      
   
endclass

  

`endif //  `ifndef __MT_MQUEUE_HOST_DRIVER_INCLUDED
