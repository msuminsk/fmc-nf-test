/*
 *  Copyright (C) 2016 CERN (www.cern.ch)
 *  Authors: Michel Arruat <michel.arruat@cern.ch>
 *  License: GPL v3
 *
 *  Common definitions used by RT code and MASTERFIP lib
 */

`ifndef __MASTERFIP_COMMON_PRIV_H
`define __MASTERFIP_COMMON_PRIV_H

`include "serializable.svh"

/* WR HMQ slots */
/* Inputs (seen from MockTurtle side) */
`define MSTRFIP_HMQ_I_CPU0_CMD 0 /* Core 0 input slot: send BA commands */
`define MSTRFIP_HMQ_I_CPU1_CMD 1 /* Core 1 input slot: send new payloads */

/* Outputs (seen from MockTurtle side) */
`define MSTRFIP_HMQ_O_UNDEFINED -1 /* undefined slot */
`define MSTRFIP_HMQ_O_APP_PER_VAR 0 /*CPU0 out slot: app.'s periodic var */
`define MSTRFIP_HMQ_O_APP_APER_VAR 1 /*CPU0 out slot: app.'s aperiodic var */
`define MSTRFIP_HMQ_O_APP_APER_MSG 2 /*CPU0 out slot: app.'s aperiodic msg */
`define MSTRFIP_HMQ_O_DIAG_PER_VAR 3 /*CPU0 out slot: diag.'s periodic var */
`define MSTRFIP_HMQ_O_DIAG_APER_VAR 4 /*CPU0 out slot: diag.'s aperiodic var */
`define MSTRFIP_HMQ_O_IRQ 5 /*CPU0 output slot: listen frame raising irq */
`define MSTRFIP_HMQ_O_RESP_CPU0_CMD 6 /* Core 0 output slot: read cmd.'s answer */
`define MSTRFIP_HMQ_O_RESP_CPU1_CMD 7 /* Core 1 output slot: read cmd.'s answer */

`define MSTRFIP_HMQ_ACQ_SLOT_COUNT 5 /* number of HMQ acquisition slots */

/* 
 * FIP frame encoding:
 * This should be left in the rt header file. Unfortunately byte operation to
 * skip some header and tail in the payload are not working properly in
 * mockturtle. Therefore some knowledge of the frame is exposed to the library.
 * frame encoding: |ctrl(1byte)|Payload(bsz)|CRC(2bytes)|
 * Payload encoding depends of the frame type:
 * MPS var payload: |PDU_type(1byte)|length(1byte)|DATA(bsz)|Status(1byte)|
 * SM_MMPS var paylaod: |PDU_type(1byte)|length(1byte)|data(bsz)|
 * APER MSG payload: |dest_addr(3bytes)|src_addr(3bytes)|data(bsz)|
 */
`define MSTRFIP_VAR_PAYLOAD_HDR_BSZ 2
`define MSTRFIP_VAR_MPS_STATUS_BSZ 1
`define MSTRFIP_MSG_PAYLOAD_HDR_BSZ 6

`define MSTRFIP_IDENT_VAR_PAYLOAD_NWORDS 3
`define MSTRFIP_PRESENCE_VAR_DATA_BSZ 5
`define MSTRFIP_PRESENCE_VAR_PAYLOAD_NWORDS 2

/**
 * MPS status definition
 * Any response frame got as last byte a status with two useful bits qualifying
 * in terms of refreshment and significance the response
 */
`define MSTRFIP_MPS_REFRESH_BIT 'h1
`define MSTRFIP_MPS_SIGNIFICANCE_BIT 'h4

`define MSTRFIP_DIAG_ADDR 0x7F
`define MSTRFIP_DIAG_PROD_VAR_ID ('h05 << 8) | `MSTRFIP_DIAG_ADDR
`define MSTRFIP_DIAG_CONS_VAR_ID ('h06 << 8) | `MSTRFIP_DIAG_ADDR
`define MSTRFIP_DIAG_VAR_DATA_BSZ 2

`define MSTRFIP_DIAG_IDENT_VAR_ID ('h0 << 8) | `MSTRFIP_DIAG_ADDR
`define MSTRFIP_AGENT_IDENT_VAR_ID 'h1000
`define MSTRFIP_IDENT_VAR_DATA_BSZ 8

/* 
 * Agent adress are in range of [0..255] (0: is the address of fipmaster.
 * 255/32 = 8 32 bits words and each bit is assigned to its corresponding agent
 * in order to register the demand for an identification variable
 * So raisng the bit [2][4] means schedule ident var of the agent address 0x64.
 * The same for presence variable
 */
`define MSTRFIP_PRESENCE_REQUEST_WSIZE 8
`define MSTRFIP_IDENT_REQUEST_WSIZE 8

/*
 * worldfip gateware defines the max payload size of HMQ messages as
 * 128 32bits words. Unfortunately the last or two words are not
 * usable (reported to Tom). So we use only 126 words
 */
`define MSTRFIP_TRTLMSG_MAX_PAYLOAD_WSZ (128 - 2)

/* 
 * FIP data attribute:
 * this flag complete the enum mstrfip_data_flags but is managed by the
 * library. If a callback has been registered with a specific FIP data, teh
 * library append this flags in order to register that an irq is required when
 * this FIP data circulates over the bus.
 */
`define MSTRFIP_DATA_FLAGS_IRQ (1 << 7)

/* MASTERFIP message's ID */
typedef enum{
	/* commands */
	MSTRFIP_CMD_GET_BITRATE = 'h1,
	MSTRFIP_CMD_SET_HW_CFG,
	MSTRFIP_CMD_SET_BA_CYCLE,
	MSTRFIP_CMD_SET_VAR_LIST,
	MSTRFIP_CMD_SET_APER_MSG,
	MSTRFIP_CMD_START_BA,
	MSTRFIP_CMD_STOP_BA,
	MSTRFIP_CMD_RESET_BA,
	MSTRFIP_CMD_SET_VAR_PAYLOAD,
	MSTRFIP_CMD_SET_MSG_PAYLOAD,
	MSTRFIP_CMD_GET_PRESENT_LIST,
	MSTRFIP_CMD_GET_REPORT,
	MSTRFIP_CMD_REQ_IDENT_VAR,
	/* answers */
	MSTRFIP_REP_ACK, /* send back when the CMD succeed */
	MSTRFIP_REP_NACK, /* send back when the CMD failed */
	MSTRFIP_REP_BITRATE
}  mstrfip_messages_id;

/* 
 * Description of every ba instruction arguments
 */

/*
 * Arguments used for a periodic var window instruction
 */
typedef struct {
	uint32_t start_var_idx; /*sorted valist index of the first var to play*/
	uint32_t stop_var_idx; /*sorted varlist index of the last var to play*/
}  mstrfip_ba_per_var_wind_args;

/*
 * Arguments used for an aperiodic var window instruction
 */
typedef struct {
	/* 
	 * time in cpu ticks, relative to the begin of the macro cycle
	 * where the aperiodic var window ends.
	 */
	uint32_t ticks_end_time;
}  mstrfip_ba_aper_var_wind_args ;

/*
 * Arguments used for an aperiodic message window instruction
 */
class mstrfip_ba_aper_msg_wind_args extends Serializable;
   
	/* 
	 * time in cpu ticks, relative to the begin of the macro cycle
	 * where the aperiodic var window ends.
	 */
	uint32_t ticks_end_time;
	/* 
	 * Define the max number of message requests sent by the agents
	 * during a periodic window in order to tell the master to schedule
	 * some message transaction during the nect aperiodic message window.
	 */
	uint32_t cons_msg_fifo_sz;
	/*
	 * Define the max number of messages sent by an application, which can
	 * be stored, waiting to be schedule in the next aperiodic message window.
	 */
   uint32_t prod_msg_fifo_sz;
   

	uint32_t prod_msg_max_bsz; /* max payload size of prod mesg (in byte) */ 
	/* 
	 * Flags defining the behaviour of the system to report on messaging.
	 * If set, an irq is raised at the end of an aperiodic window to report
	 * on consumed or/and produced messages that occurred during this
	 * window.
	 */
	uint32_t cons_msg_global_irq;
	uint32_t prod_msg_global_irq;


   virtual     task automatic serialize( ByteBuffer data );
      data.addWord (ticks_end_time);
      data.addWord (cons_msg_fifo_sz);
      data.addWord (prod_msg_fifo_sz);
      data.addWord (prod_msg_max_bsz);
      data.addWord (cons_msg_global_irq);
      data.addWord (prod_msg_global_irq);
   endtask
   
   
endclass // mstrfip_ba_aper_msg_wind_args


/*
 * Arguments used for a wait window instruction
 */
typedef struct{
	uint32_t ticks_end_time;
	uint32_t is_silent;
} mstrfip_ba_wait_wind_args;

/**
 * @enum mstrfip_ba_instruction_set
 * The full instruction set supported by the master. It's a subset of teh
 * original FIP's instruction set
 */
typedef enum {
	MSTRFIP_BA_PER_VAR_WIND = 1, /**< process periodic variables */
	MSTRFIP_BA_APER_MSG_WIND, /**< process aperiodic messages */
	MSTRFIP_BA_APER_VAR_WIND, /**< process aperiodic SMMPS variables */
	MSTRFIP_BA_WAIT_WIND, /**< wait until an absolute time in the macro cycle*/
	MSTRFIP_BA_NEXT_MACRO /**< start again the macro cycle execution */
} mstrfip_ba_instr_code;

/*
 * In order to program the macro cycle we use the concept of instruction. An
 * instruction is made of code and a set of arguments which are depending of
 * the instruction codde.
 */
typedef struct{
	mstrfip_ba_instr_code code;
	mstrfip_ba_per_var_wind_args per_var_wind;
	mstrfip_ba_aper_var_wind_args aper_var_wind;
	mstrfip_ba_aper_msg_wind_args aper_msg_wind;
	mstrfip_ba_wait_wind_args wait_wind;
} mstrfip_ba_instr;

/*
 * Any mockturtle msg contains this header
 */
class mstrfip_trtlmsg_hdr extends Serializable;
   uint32_t id; /* message id */
   uint32_t seq; /* sequence number */

   function new ( uint32_t id_ = 0, uint32_t seq_ = 0);
      id = id_;
      seq = seq_;
   endfunction // new
   
   
   virtual    task automatic serialize( ByteBuffer data );
      data.addWord (id);
      data.addWord (seq);
   endtask // serialize

   virtual    task automatic deserialize( ByteBuffer data );
      id = data.getWord ();
      seq = data.getWord ();
   endtask // serialize

   
   
endclass // mstrfip_trtlmsg_hdr

/*
 * mturtle message header used to program the macro cycle.
 * This header is then followed by the payload which is a list of
 * struct_ba_instr entries. instr_count field gives the
 * number of instructions contained in the payload.
 *
 */
`ifdef disabled
typedef struct {
	mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t cycle_ticks_length; /* macro cycle duration in CPU ticks */
	uint32_t instr_count; /* number of instruction */
} mstrfip_ba_instr_hdr_trtlmsg ;
`endif


class mstrfip_hw_speed_trtlmsg extends Serializable;
   mstrfip_trtlmsg_hdr trtl_hdr;
   uint32_t bitrate; /* rx/tx bit rate */

   function  new;
      trtl_hdr = new;      
   endfunction // serialize
   
   virtual     task automatic serialize( ByteBuffer data );
      trtl_hdr.serialize(data);
      data.addWord (bitrate);
   endtask // serialize

   virtual     task automatic deserialize( ByteBuffer data );
      trtl_hdr.deserialize(data);
      bitrate = data.getWord();
   endtask // serialize


   
endclass // mstrfip_trtlmsg_hdr


/*
 * mturtle message used to configure HW
 */

class mstrfip_hw_cfg_trtlmsg extends Serializable;
   
   mstrfip_trtlmsg_hdr trtl_hdr;
   uint32_t enb_ext_trig; /* enable external trigger */
   uint32_t enb_ext_trig_term; /* enable external trigger 50ohms term */
   uint32_t enb_int_trig; /* enable internal trigger */
   uint32_t tr_ticks; /* turn around time in CPU ticks */
   uint32_t ts_ticks; /* silence time in CPU ticks */
   uint32_t bit_ticks; /* bit tx/rx speed */

   function new;
      trtl_hdr = new;
   endfunction
   
   virtual     task automatic serialize( ByteBuffer data );
      trtl_hdr.serialize(data);
      data.addWord (enb_ext_trig);
      data.addWord (enb_ext_trig_term);
      data.addWord (enb_int_trig);
      data.addWord (tr_ticks);
      data.addWord (ts_ticks);
      data.addWord (bit_ticks);
   endtask // serialize
   
endclass // mstrfip_hw_cfg_trtlmsg

`ifdef disabled

/*
 * mturtle message used to set var payload
 */
typedef struct {
	mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t key; /**< used to locate corresponding fip data object */
	uint32_t bsz;  /**< payload size in bytes */
} mstrfip_var_payload_trtlmsg;

/*
 * mturtle message used to set msg payload
 */
typedef struct {
	mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t dest_addr; /**< address of the remote agent */
	uint32_t bsz; /**< payload size in bytes */
	uint32_t flags; /**< direction + irq flags */ 
} mstrfip_msg_payload_trtlmsg;
#define MSTRFIP_MSG_PAYLOAD_HDR_TRTLMSG_WSZ \
		sizeof(struct mstrfip_msg_payload_trtlmsg) / sizeof(uint32_t)

/*
 * mturtle message used to request identification var
 */
struct mstrfip_ident_request_trtlmsg {
	struct mstrfip_trtlmsg_hdr trtl_hdr;
	uint32_t ident_request[MSTRFIP_IDENT_REQUEST_WSIZE];
};
#define MSTRFIP_MSG_IDENT_REQUEST_TRTLMSG_WSZ \
		sizeof(struct mstrfip_ident_request_trtlmsg) / sizeof(uint32_t)

/*
 * This struct is shared by the library and the RT
 * Note: basically, key is used for efficient lookup in the lib and in the  
 * RT task to retrieve the appropriate data structure, through messages
 * exchanged between host and mock-turtle.
 * Alignment issue: because this struct is exchanged with the rt side the order
 * of fields respect the self-alignment rule. The sizeof struct mstrfip_var_desc
 * gives 8 bytes (2 32bits word)on an x86 processor
 */
struct mstrfip_data_hdr {
	uint16_t key; /**< used to locate efficiently fip data */
	uint16_t id; /**< var id 0xXXYY : XX=var number YY:agent adress */
	uint16_t max_bsz; /**< max payload size in bytes (max=256bytes) */
	uint8_t mq_slot; /**< acq mq slot */
	uint8_t flags; /**< direction + IRQ flag */
};

`endif //  `ifdef disabled

`endif //  `ifndef __MASTERFIP_COMMON_PRIV_H

