`ifndef __FIP_VIRTUAL_MASTER_SVH
 `define __FIP_VIRTUAL_MASTER_SVH

 `include "fip_frame.svh"
 `include "fip_device.svh"


class FipVirtualTransfer;
   protected bit m_is_send;
   protected FipFrame m_frame;

   function new ( bit isSend, FipFrame theFrame = null );

      m_is_send = isSend;
      m_frame = theFrame;
   endfunction // new

   function automatic bit isSend();
      return m_is_send;
   endfunction // isSend

   function automatic FipFrame getFrame();
      return m_frame;
   endfunction // getFrame

   task automatic setFrame( FipFrame frame );
      m_frame = frame;
   endtask // setFrame
   
endclass

class Timeout;

   protected time m_length, m_start_time;
   
   function new ( time length );
      m_length = length;
      m_start_time = $time;
      
   endfunction // new

   function automatic int expired();
      return ($time-m_start_time) >= m_length;
   endfunction // expired

   task automatic waitExpire();

      while(!expired())
	#100ns;
      
   endtask // waitExpire
   
     
   
endclass // Timeout


class FipVirtualMaster extends FipDevice;

   protected FipVirtualTransfer m_xfers[$];
   protected time m_silenceTime, m_turnaroundTime;
   protected FipFrame m_lastRxFrame;
   protected bit  m_rxFrameReady, m_rxFramePreambleReady;
      
   function new;
      super.new;
   endfunction // setTiming
   
   
   task automatic setTiming( time silenceTime, time turnaroundTime);
      m_silenceTime = silenceTime;
      m_turnaroundTime = turnaroundTime;
   endtask // setTiming
   
   task automatic requestSend( FipFrame frame );
      FipVirtualTransfer xfer = new( 1, frame );
      
      m_xfers.push_back(xfer);
   endtask // requestSend

   task automatic expectRecv( int count );
      int i;

      for(i=0;i<count;i++)
	begin
	   FipVirtualTransfer xfer = new (0);
	   m_xfers.push_back( xfer );
	end
      
   endtask // expectRecv

   task automatic onReceive( FipFrame frame );
//      $display("********************OnREceive [%x %x]", this, frame.getControl());
      
      m_lastRxFrame = frame.copy();
      m_rxFrameReady = 1;

   endtask // onReceive

   task automatic onPreamble( );
      m_rxFramePreambleReady = 1;
   endtask // onPreamble
   
   
   task automatic waitTxIdle();
      while(!isIdle())
	#100ns;
   endtask

   task automatic runMacrocycle();
      int i;

      FipVirtualTransfer xfer;
      
      for (i=0;i<m_xfers.size(); i++)
	begin
	   automatic int prevIsSend = 0;
	   automatic int prevIsExpect = 0;
	   automatic int nextIsSend = 0;

	   if( i > 1 )
	     prevIsSend = m_xfers[i-1].isSend();

	   if( i > 1 )
	     prevIsExpect = !m_xfers[i-1].isSend();

	   if( i < m_xfers.size() - 1)
	     nextIsSend = m_xfers[i+1].isSend();

	   
	   
	   if( m_xfers[i].isSend() )
	     begin
		FipFrame frame = m_xfers[i].getFrame();
		
//		$display("VM: Sending %d/%d", i+1,  m_xfers.size(), frame.size(), nextIsSend);

		if( prevIsExpect )
		  begin
		     #(m_turnaroundTime);
		  end

		send( frame ); // non-blocking

		if( nextIsSend )
		  begin
		     waitTxIdle();
		     #(m_turnaroundTime);
		  end
		
	     end else begin // if ( m_xfers[i].isSend() )
		// we expect a frame
		Timeout tmo = new(m_silenceTime);
		m_rxFrameReady = 0;
		m_rxFramePreambleReady = 0;

		
		while( !tmo.expired() )
		  begin
		     
		     if( m_rxFramePreambleReady )
		       begin
			  
			  while (! m_rxFrameReady )
			    #100ns;
			  
			  m_xfers[i].setFrame(m_lastRxFrame);
			  
			  break;
		       end
		     
		     #100ns;
		  end

		if(tmo.expired() && !m_rxFrameReady )
		  begin
		     
//		     $error("Didn't receive expected frame\n");
		  end

		m_rxFrameReady = 0;
		m_rxFramePreambleReady = 0;
		
	     end // else: !if( m_xfers[i].isSend() )
	   
	end

      waitTxIdle();
      

      
   endtask // runMacrocycle

   virtual task automatic configure();
   endtask // configure
   
      
	
endclass // FipVirtualMaster

class FipTestVirtualMaster extends FipVirtualMaster;

   function new;
      super.new;
   endfunction // setTiming

   task automatic configure();
      int i;


   
      // periodic vars

      // ID_DAT (0x0101) + RP_DAT (master produces)
      requestSend( FipFrame::makeRaw( '{'h03, 'h01, 'h01 }) ); 
      requestSend( FipFrame::makeRaw( '{'h02, 'h40, 'h41, 'h00, 'h01, 'h02, 'h03, 'h04, 'h05, 'h06, 'h07, 'h08, 'h09, 'h0a, 'h0b, 'h0c, 'h0d, 'h0e, 'h0f, 'h10, 'h11, 'h12, 'h13, 'h14, 'h15, 'h16, 'h17, 'h18, 'h19, 'h1a, 'h1b, 'h1c, 'h1d, 'h1e, 'h1f, 'h20, 'h21, 'h22, 'h23, 'h24, 'h25, 'h26, 'h27, 'h28, 'h29, 'h2a, 'h2b, 'h2c, 'h2d, 'h2e, 'h2f, 'h30, 'h31, 'h32, 'h33, 'h34, 'h35, 'h36, 'h37, 'h38, 'h39, 'h3a, 'h3b, 'h3c, 'h3d, 'h3e, 'h3f, 'h05 } ) );

      
      // ID_DAT (0x015f) + RP_DAT (master produces)
      requestSend( FipFrame::makeRaw( '{ 'h03, 'h01, 'h5f }) );
      requestSend( FipFrame::makeRaw( '{ 'h02, 'h40, 'h03, 'h00, 'h01, 'h05 }) );

      
      // ID_DAT (0x01b4) + RP_DAT (master produces)
      requestSend( FipFrame::makeRaw( '{  'h03, 'h01, 'hb4, 'h3e, 'h70 } ) );
      requestSend( FipFrame::makeRaw( '{  'h02, 'h40, 'h7d, 'h00, 'h01, 'h02, 'h03, 'h04, 'h05, 'h06, 'h07, 'h08, 'h09, 'h0a, 'h0b, 'h0c, 'h0d, 'h0e, 'h0f, 'h10, 'h11, 'h12, 'h13, 'h14, 'h15, 'h16, 'h17, 'h18, 'h19, 'h1a, 'h1b, 'h1c, 'h1d, 'h1e, 'h1f, 'h20, 'h21, 'h22, 'h23, 'h24, 'h25, 'h26, 'h27, 'h28, 'h29, 'h2a, 'h2b, 'h2c, 'h2d, 'h2e, 'h2f, 'h30, 'h31, 'h32, 'h33, 'h34, 'h35, 'h36, 'h37, 'h38, 'h39, 'h3a, 'h3b, 'h3c, 'h3d, 'h3e, 'h3f, 'h40, 'h41, 'h42, 'h43, 'h44, 'h45, 'h46, 'h47, 'h48, 'h49, 'h4a, 'h4b, 'h4c, 'h4d, 'h4e, 'h4f, 'h50, 'h51, 'h52, 'h53, 'h54, 'h55, 'h56, 'h57, 'h58, 'h59, 'h5a, 'h5b, 'h5c, 'h5d, 'h5e, 'h5f, 'h60, 'h61, 'h62, 'h63, 'h64, 'h65, 'h66, 'h67, 'h68, 'h69, 'h6a, 'h6b, 'h6c, 'h6d, 'h6e, 'h6f, 'h70, 'h71, 'h72, 'h73, 'h74, 'h75, 'h76, 'h77, 'h78, 'h79, 'h7a, 'h7b, 'h05} ) );



      
      // ID_DAT (0x0201) + expect RP_DAT (slave 0x1 produces)
      requestSend( FipFrame::makeRaw( '{ 'h03, 'h02, 'h01 } ) );
      expectRecv(1);

      // ID_DAT (0x025f) + expect RP_DAT (slave 0x5f produces)
      requestSend( FipFrame::makeRaw( '{ 'h03, 'h02, 'h5f } ) );
      expectRecv(1);

      // ID_DAT (0x02b4) + expect RP_DAT (slave 0xb4 produces)
      requestSend( FipFrame::makeRaw( '{ 'h03, 'h02, 'hb4 } ) );
      expectRecv(1);

      //aperiodic vars

      // ID_MSG( 0201), expect RP_MSG_DAT + RP_FIN
      requestSend( FipFrame::makeRaw( '{'h05, 'h02, 'h01 }) ); 
      expectRecv(2);

      // ID_MSG( 0001 ), send RP_MSG_DAT + RP_FIN
      requestSend( FipFrame::makeRaw( '{'h05, 'h00, 'h01 }) );
      requestSend( FipFrame::makeRP_MSG( FT_RP_DAT_MSG, 'h0, 'h400, makeRange(0, 255 ) ) );
      requestSend( FipFrame::makeRP_FIN( ) );


      // SMMPS vars

      for(i = 0; i <256; i++)
	begin
	   uint8_t payload [$] = '{ 'h03, 'h14, 0 };
	   payload[2]= i;
	   
	   // ID_DAT: SMMPS frame
	   requestSend( FipFrame::makeRaw(payload));
	   expectRecv(1);
	end
      

      
   endtask
      
endclass // FipTestVirtualMaster

   

`endif
