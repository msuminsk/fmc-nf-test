/*
 * This program source code file is part of MasterFip project.
 *
 * Copyright (C) 2013-2017 CERN
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

/* 
   fip_phy_model.svh - implementation of the FipPHY interface,
   an approximate model a WorldFIP PHY. The functionality covered
   is:
    - bit serialization and deserialization
    - CRC calculation and check
    - FSS/FES insertion and check
    - reproduction of fault conditions (truncated preambles, wrong CRCs, etc)
 */

`ifndef __FIP_PHY_MODEL_INCLUDED
`define __FIP_PHY_MODEL_INCLUDED

`include "fip_frame.svh"
`include "fip_device.svh"

typedef class FipDevice;

interface FipPHY
  (
   inout fip_p,
   inout fip_n
  );

   time  cfg_bit_time = 200ns;
   time  cfg_sampling_clock_period = 25ns;
   time  cfg_turnaround_time = 32us;
   time  cfg_silence_time = 116us;
   int 	 cfg_clocks_per_bit = (cfg_bit_time / cfg_sampling_clock_period);
   int 	 cfg_verbose = 0;
   

   task automatic setVerbose( int verbose );
      cfg_verbose = verbose;
   endtask // setVerbose
   
   task automatic setSpeed( int speed );
      case(speed)
	31250:
	  begin
	     cfg_bit_time = 16us;
	     cfg_turnaround_time = 480us;
	     cfg_silence_time = 4096us;
	  end
	
	1000000:
	  begin
	     cfg_bit_time = 0.5us;
	     cfg_turnaround_time = 14us;
	     cfg_silence_time = 150us;
	  end
	
	2500000: 
	  begin
	   cfg_bit_time = 0.2us;
	   cfg_turnaround_time = 13.5us;
	   cfg_silence_time = 96us;
	end
	
      endcase // case (speed)

      cfg_clocks_per_bit = cfg_bit_time / cfg_sampling_clock_period;

      
   endtask // setTiming

   function automatic time getTurnaroundTime();
      return cfg_turnaround_time;
   endfunction // getTurnaroundTime

   function automatic time getSilenceTime ();
      return cfg_silence_time;
   endfunction // getSilenceTime
   
   FipDevice handler;

   reg 	 fip_d = 0;
   reg 	 clk = 0;
   reg 	 fip_txd = 0;
   reg 	 fip_txen = 0;
   int 	 sample_cnt;
   wire  fip_i = fip_p;
   
   always #(cfg_sampling_clock_period/2) clk <= ~clk;

   assign fip_p = (fip_txen ? fip_txd : 1'bz);
   assign fip_n = (fip_txen ? ~fip_txd : 1'bz);

   
   always@(posedge clk)
     begin
	fip_d <= fip_i;
	if(fip_d ^ fip_i)
	  sample_cnt <= 0;
	else if (sample_cnt == cfg_clocks_per_bit - 1)
	  sample_cnt <= 0;
	else
	  sample_cnt <= sample_cnt + 1;
     end
   
   wire sample_p = (sample_cnt == (cfg_clocks_per_bit / 2));
   reg 	sample_p_d;
   
   reg [27:0] bits;

   int 	      bit_count = 0;

// shift register sampling the data   
   always@(posedge clk)
     begin
	if(fip_txen)
	  begin
	     bits <= 0;
	     bit_count <= 0;
	  end else begin
	     sample_p_d <= sample_p;
	
	     if(sample_p)
	       begin
		  bits <= { bits[26:0], fip_i};
		  bit_count = bit_count + 1;
	       end
	  end // else: !if(fip_txen)
     end

`define FIP_FSS_BITS 28'b1001100110011011001001001101
`define FIP_FES_BITS 16'b1011001100100110

`define ST_IDLE 0
`define ST_FRAME 1

   int 	      state = `ST_IDLE;
   bit [7:0]  rx_data;
   FipFrame tx_queue[$];

   wire       rx_idle ;


   assign rx_idle = (fip_p == 1'h0 && fip_n == 1'h0);
 //(fip_p != 1'b0) ? 1'b1 : 1'b0;
// (fip_p != 1'b0 && fip_p != 1'b1 && fip_n != 1'b0 && fip_n != 1'b1 && fip_p != 1'bx && fip_n != 1'bx);
   
   
   uint8_t frame_data[$];
   int 	      frame_size = 0;
   
   int count = 0;
   time start_t, end_t;

   time lastRxEndTime = 0;
   time lastTxEndTime = 0;
 
   always@(posedge clk)
     begin
	case (state)
	  `ST_IDLE: begin
	     if (bits == `FIP_FSS_BITS && sample_p_d)
	       begin
		frame_data='{};

	     start_t = $time - cfg_bit_time * 32;
	     
	     bit_count = 0;
	     state = `ST_FRAME;
	     frame_size = 0;
	     count <= 0;

	     if (cfg_verbose)
	       $display("[PHY]: RX FSS");
	     
	     if (handler)
	       handler.onPreamble ();

	     
	     
	  end
	     
	  end
	  
	  `ST_FRAME: begin

	       if (bits[15:0] == `FIP_FES_BITS && sample_p_d)
		 begin
		    automatic FipFrame frame = new;
		 //   automatic ByteBuffer b = new ( frame_data );


		    if (cfg_verbose)
		      $display("[PHY]: RX FES");

		    
		    lastRxEndTime = $time;
		    frame.setStartTime(start_t);
   		    frame.m_end_time = $time;
		    frame.deserializeBytes( frame_data );
		    
		    if (handler)
		      handler.onReceive (frame);

		  state <= `ST_IDLE;
		  
	       end else
	     if(sample_p_d && bit_count == 16)
	       begin
		  if(bits == 0 || bits == 'hffff)
		    begin
		       if (cfg_verbose)
			 $display("[PHY]: RX Timeout");
		       state <= `ST_IDLE;
		    end else begin
		  
		  
		  rx_data[7] = bits[15];
		  rx_data[6] = bits[13];
		  rx_data[5] = bits[11];
		  rx_data[4] = bits[9];
		  rx_data[3] = bits[7];
		  rx_data[2] = bits[5];
		  rx_data[1] = bits[3];
		  rx_data[0] = bits[1]; // fucked-up manchester decoder ;)
		  
		  bit_count = 0;

		    if (cfg_verbose)
		      $display("[PHY]: RX 0x%x", rx_data);
		  
		  frame_data[frame_size++] = rx_data;
		    end // else: !if(bits == 0 || bits = 'hffff)
		  
	       end
	  end
	endcase // case (state)
     end


   bit tx_idle = 1;
   wire idle = tx_idle && rx_idle;
   
   

   task automatic waitTurnaround( FipFrame f );
      automatic time target = cfg_turnaround_time;
      
      if(f.getForceTxTimeOffset() )
	   target += f.getTxTimeOffset();
      
      while  ( ( ($time - lastRxEndTime) < target) || (($time-lastTxEndTime) < target) )
	#1us;
   endtask // waitTurnaround
   
   initial forever begin
      while ( tx_queue.size() ) 
	begin
	   automatic FipFrame frame = tx_queue.pop_front();

	   waitTurnaround( frame );
	   tx_idle = 0;

	   sendFrame(frame);
	   lastTxEndTime = $time;
	end
      
      @(posedge clk);
      tx_idle = 1;
   end
   
   
/* 
 
 Transmission part 
 
 */
   
 
   task automatic frame_symbol( ref bit bits[$], input int sym);
      case (sym)
	0: begin
	   bits.push_back( 0 );
	   bits.push_back( 1 );
	end
	1: begin
	   bits.push_back( 1 );
	   bits.push_back( 0 );
	end
	`VIOL_PLUS: begin
	   bits.push_back( 1 );
	   bits.push_back( 1 );
	end
	`VIOL_MINUS: begin
	   bits.push_back( 0 );
	   bits.push_back( 0 );
	end
      endcase // case (sym)
   endtask // frame_symbol

   task automatic frame_preamble(ref bit bits[$], input int forceError, input int bitCount);
      automatic bit preBits[$] = '{1,0,1,0,1,0,1,0};
      
      if (forceError)
	begin
	    for(int i = 0; i < 8; i ++)
	      frame_symbol(bits, ~preBits[i]); // preamble
	end else begin
	   
	   for(int i = 8-bitCount; i < 8 ;i++)
	     frame_symbol(bits, preBits[i]); // preamble

	end // else: !if(forceError)
      
      frame_symbol(bits, 1); // SFD
      frame_symbol(bits, `VIOL_PLUS);
      frame_symbol(bits, `VIOL_MINUS);
      frame_symbol(bits, 1);
      frame_symbol(bits, 0);
      frame_symbol(bits, `VIOL_MINUS);
      frame_symbol(bits, `VIOL_PLUS);
      frame_symbol(bits, 0);
   endtask // frame_preamble

   task automatic frame_fes(ref bit bits[$], input int forceError, input int bitCount);

      if(forceError)
	begin
	   for(int i = 0; i < 8; i++)
	     frame_symbol( bits, i % 2 ? 1 : 0);

	   return;
	end
       
      if(bitCount == 0)
	return;

      frame_symbol(bits, 1); // FE seq

      if(bitCount == 1)
	return;
      
      frame_symbol(bits, `VIOL_PLUS);

      if(bitCount == 2)
	return;

      frame_symbol(bits, `VIOL_MINUS);

      if(bitCount == 3)
	return;

      frame_symbol(bits, `VIOL_PLUS);

      if(bitCount == 4)
	return;

      frame_symbol(bits, `VIOL_MINUS);

      if(bitCount == 5)
	return;

      frame_symbol(bits, 1);

      if(bitCount == 6)
	return;

      frame_symbol(bits, 0);

      if(bitCount == 7)
	return;

      frame_symbol(bits, 1);

   endtask // frame_fes

   task automatic frame_byte(ref bit bits[$], input byte b, input int bitCount = 8);
      int i;
      for(i=0;i<bitCount;i++)
	frame_symbol(bits, b & (1<<(7-i)) ? 1 : 0);
	   
  endtask // frame_byte

   

   task automatic sendFrame ( FipFrame frame );
      automatic       int fcs;
      automatic       int tmp;
      automatic       bit bits[$];
      automatic       int size, i;
      automatic ByteBuffer b = new;
      automatic bit terminated = 0;
      automatic int nBits;
      
      
      frame.serialize(b);

      fcs = 'hffff;
      nBits = b.data.size() * 8;

      if (frame.getForceDataTxBitCount())
	nBits = frame.getDataTxBitCount();
            
      frame_preamble(bits, frame.getForcePreambleError(), frame.getPreambleBitCount() );

      for ( i = 0; i < b.data.size(); i++)
	begin
	   automatic int cnt = (nBits - i*8);

	   if( cnt > 8 )
	     cnt = 8;

	   frame_byte(bits, b.data[i], cnt);
	   crc16_update(fcs, b.data[i], cnt);
	   

	   if( cnt < 8 && frame.getForcePrematureEnd() )
	     begin
		terminated = 1;
		break;
	     end
	end
      

      if(!terminated)
	begin

	   if ( frame.getForceIncorrectCRC() )
	     begin
		frame_byte(bits, ((~revbits16(fcs)) >> 8) + 1);
		frame_byte(bits, ((~revbits16(fcs)) & 'hff) + 1);
	     end else begin
		frame_byte(bits, (~revbits16(fcs)) >> 8);
		frame_byte(bits, (~revbits16(fcs)) & 'hff);
	     end
	   

	   tmp = fcs;

	   crc16_update(fcs,  (~revbits16(tmp)) >> 8);
	   crc16_update(fcs,  (~revbits16(tmp)) & 'hff);

	   frame_fes(bits, frame.getForceFESError(), frame.getFESBitCount() );
	end // if (!terminated)
      

      fip_txen <= 'h1;
      #1us;
      
      foreach ( bits[i] )
	begin
	   fip_txd <= bits[i];
	   #(cfg_bit_time);
	   
	end

      #1us;
      fip_txen <= 0;
      #0us;
      
   endtask // send

   task automatic setHandler ( FipDevice handler_ );
      handler = handler_;
   endtask // setHandler
   
     
   task automatic send ( FipFrame frame );
      tx_idle = 0;
      tx_queue.push_back(frame);
   endtask // send

   function automatic bit isIdle();
      return tx_idle && rx_idle;
   endfunction // idle
   
   

endinterface // FipPHY



`endif //  `ifndef __FIP_PHY_MODEL_INCLUDED

