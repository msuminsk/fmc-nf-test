`timescale 1 ps / 1 ps

module vcc(output y);
  assign (supply1,strong0) y = 1;
endmodule

module gnd(output y);
  assign (strong1,supply0) y = 0;
endmodule

module ram4k9 (
      addra11, addra10, addra9, addra8, addra7, addra6,
      addra5, addra4, addra3, addra2, addra1, addra0,
      addrb11, addrb10, addrb9, addrb8, addrb7, addrb6,
      addrb5, addrb4, addrb3, addrb2, addrb1, addrb0,
      dina8, dina7, dina6, dina5, dina4, dina3, dina2, dina1, dina0,
      dinb8, dinb7, dinb6, dinb5, dinb4, dinb3, dinb2, dinb1, dinb0,
      widtha0, widtha1,
      widthb0, widthb1,
      pipea, pipeb,
      wmodea, wmodeb,
      blka, blkb,
      wena, wenb,
      clka, clkb,
      reset,
      douta8, douta7, douta6, douta5, douta4, douta3, douta2, douta1, douta0,
      doutb8, doutb7, doutb6, doutb5, doutb4, doutb3, doutb2, doutb1, doutb0
);

parameter delay_two = 2;
parameter tc2cwwl = 331;
parameter tc2cwwh = 303;
parameter tc2crwh = 452;
parameter tc2cwrh = 493;

input addra11, addra10, addra9, addra8, addra7, addra6, addra5, addra4, addra3, addra2, addra1, addra0;
input dina8, dina7, dina6, dina5, dina4, dina3, dina2, dina1, dina0;
input widtha1, widtha0, pipea, wmodea, blka, wena, clka;
input addrb11, addrb10, addrb9, addrb8, addrb7, addrb6, addrb5, addrb4, addrb3, addrb2, addrb1, addrb0;
input dinb8, dinb7, dinb6, dinb5, dinb4, dinb3, dinb2, dinb1, dinb0;
input widthb1, widthb0, pipeb, wmodeb, blkb, wenb, clkb, reset;

output douta8, douta7, douta6, douta5, douta4, douta3, douta2, douta1, douta0;
output doutb8, doutb7, doutb6, doutb5, doutb4, doutb3, doutb2, doutb1, doutb0;


reg doutap8, doutap7, doutap6, doutap5, doutap4, doutap3, doutap2, doutap1, doutap0;
reg doutbp8, doutbp7, doutbp6, doutbp5, doutbp4, doutbp3, doutbp2, doutbp1, doutbp0;

reg doutap8_stg1, doutap7_stg1, doutap6_stg1, doutap5_stg1, doutap4_stg1, doutap3_stg1, doutap2_stg1, doutap1_stg1, doutap0_stg1;
reg doutbp8_stg1, doutbp7_stg1, doutbp6_stg1, doutbp5_stg1, doutbp4_stg1, doutbp3_stg1, doutbp2_stg1, doutbp1_stg1, doutbp0_stg1;

wire clka_int, clkb_int;
wire wena_int, wenb_int, wmodea_int, wmodeb_int;
wire blka_int, blkb_int, reset_int,  pipea_int, pipeb_int;

wire addra11_int, addra10_int, addra9_int, addra8_int, addra7_int, addra6_int, addra5_int, addra4_int;
wire addra3_int, addra2_int, addra1_int, addra0_int;

wire addrb11_int, addrb10_int, addrb9_int, addrb8_int, addrb7_int, addrb6_int, addrb5_int, addrb4_int;
wire addrb3_int, addrb2_int, addrb1_int, addrb0_int;

wire dina8_int, dina7_int, dina6_int, dina5_int, dina4_int, dina3_int, dina2_int, dina1_int, dina0_int;
wire dinb8_int, dinb7_int, dinb6_int, dinb5_int, dinb4_int, dinb3_int, dinb2_int, dinb1_int, dinb0_int;

reg [8:0] mem_512_9 [0:511];

reg notify_reg;

integer addra;            // address of port a
integer addrb;            // address of port b
integer depth;
integer maxadd;

reg  addra_valid;
reg  addrb_valid;

wire blka_en;
wire blkb_en;

wire blka_wen;
wire blkb_wen;

time clka_wr_re, clka_rd_re;
time clka_wr_fe, clka_rd_fe;
time clkb_wr_re, clkb_rd_re;
time clkb_wr_fe, clkb_rd_fe;

reg  wena_lat, wenb_lat;
reg  blka_lat, blkb_lat;

reg  dina8_reg, dina7_reg, dina6_reg, dina5_reg, dina4_reg, dina3_reg, dina2_reg, dina1_reg, dina0_reg;
reg  dinb8_reg, dinb7_reg, dinb6_reg, dinb5_reg, dinb4_reg, dinb3_reg, dinb2_reg, dinb1_reg, dinb0_reg;

reg  dina8_bypass, dina7_bypass, dina6_bypass, dina5_bypass, dina4_bypass;
reg  dina3_bypass, dina2_bypass, dina1_bypass, dina0_bypass;

reg  dinb8_bypass, dinb7_bypass, dinb6_bypass, dinb5_bypass, dinb4_bypass;
reg  dinb3_bypass, dinb2_bypass, dinb1_bypass, dinb0_bypass;


/********************* text macro definitions ******************/

`define blka_width_cfg {widtha1,widtha0}
`define blkb_width_cfg {widthb1,widthb0}

`define blka_addr      {addra11_int,addra10_int,addra9_int,addra8_int,addra7_int,addra6_int,addra5_int,addra4_int,addra3_int,addra2_int,addra1_int,addra0_int}
`define blkb_addr      {addrb11_int,addrb10_int,addrb9_int,addrb8_int,addrb7_int,addrb6_int,addrb5_int,addrb4_int,addrb3_int,addrb2_int,addrb1_int,addrb0_int}

`define data_a_out     {doutap8,doutap7,doutap6,doutap5,doutap4,doutap3,doutap2,doutap1,doutap0}
`define data_b_out     {doutbp8,doutbp7,doutbp6,doutbp5,doutbp4,doutbp3,doutbp2,doutbp1,doutbp0}

`define datap_a_out    {doutap8_stg1,doutap7_stg1,doutap6_stg1,doutap5_stg1,doutap4_stg1,doutap3_stg1,doutap2_stg1,doutap1_stg1,doutap0_stg1}
`define datap_b_out    {doutbp8_stg1,doutbp7_stg1,doutbp6_stg1,doutbp5_stg1,doutbp4_stg1,doutbp3_stg1,doutbp2_stg1,doutbp1_stg1,doutbp0_stg1}

`define data_a_in      {dina8_int,dina7_int,dina6_int,dina5_int,dina4_int,dina3_int,dina2_int,dina1_int,dina0_int}
`define data_b_in      {dinb8_int,dinb7_int,dinb6_int,dinb5_int,dinb4_int,dinb3_int,dinb2_int,dinb1_int,dinb0_int}

`define data_a_reg     {dina8_reg,dina7_reg,dina6_reg,dina5_reg,dina4_reg,dina3_reg,dina2_reg,dina1_reg,dina0_reg}
`define data_b_reg     {dinb8_reg,dinb7_reg,dinb6_reg,dinb5_reg,dinb4_reg,dinb3_reg,dinb2_reg,dinb1_reg,dinb0_reg}

`define data_a_byp     {dina8_bypass,dina7_bypass,dina6_bypass,dina5_bypass,dina4_bypass,dina3_bypass,dina2_bypass,dina1_bypass,dina0_bypass}
`define data_b_byp     {dinb8_bypass,dinb7_bypass,dinb6_bypass,dinb5_bypass,dinb4_bypass,dinb3_bypass,dinb2_bypass,dinb1_bypass,dinb0_bypass}

assign blka_en = ~blka_int & reset_int;
assign blkb_en = ~blkb_int & reset_int;
assign blka_wen = ~blka_int & reset_int & ~wena_int;
assign blkb_wen = ~blkb_int & reset_int & ~wenb_int;

//`define clka_int clka
//`define clkb_int clkb
//`define wena_int wena
//`define wenb_int wenb
buf b0(clka_int, clka);
buf b1(clkb_int, clkb);
buf b2(wena_int, wena);
buf b3(wenb_int, wenb);
//assign clka_int = clka;
//assign clkb_int = clkb;
//assign wena_int = wena;
//assign wenb_int = wenb;

buf b4(dina0_int, dina0);
buf b5(dina1_int, dina1);
buf b6(dina2_int, dina2);
buf b7(dina3_int, dina3);
buf b8(dina4_int, dina4);
buf b9(dina5_int, dina5);
buf b10(dina6_int, dina6);
buf b11(dina7_int, dina7);
buf b12(dina8_int, dina8);


buf b13(dinb0_int, dinb0);
buf b14(dinb1_int, dinb1);
buf b15(dinb2_int, dinb2);
buf b16(dinb3_int, dinb3);
buf b17(dinb4_int, dinb4);
buf b18(dinb5_int, dinb5);
buf b19(dinb6_int, dinb6);
buf b20(dinb7_int, dinb7);
buf b21(dinb8_int, dinb8);

buf b22(addra0_int, addra0);
buf b23(addra1_int, addra1);
buf b24(addra2_int, addra2);
buf b25(addra3_int, addra3);
buf b26(addra4_int, addra4);
buf b27(addra5_int, addra5);
buf b28(addra6_int, addra6);
buf b29(addra7_int, addra7);
buf b30(addra8_int, addra8);
buf b31(addra9_int, addra9);
buf b32(addra10_int, addra10);
buf b33(addra11_int, addra11);

buf b34(addrb0_int, addrb0);
buf b35(addrb1_int, addrb1);
buf b36(addrb2_int, addrb2);
buf b37(addrb3_int, addrb3);
buf b38(addrb4_int, addrb4);
buf b39(addrb5_int, addrb5);
buf b40(addrb6_int, addrb6);
buf b41(addrb7_int, addrb7);
buf b42(addrb8_int, addrb8);
buf b43(addrb9_int, addrb9);
buf b44(addrb10_int, addrb10);
buf b45(addrb11_int, addrb11);

buf b46(reset_int, reset);
buf b47(blka_int, blka);
buf b48(blkb_int, blkb);
buf b49(pipea_int, pipea);
buf b50(pipeb_int, pipeb);
buf b51(wmodea_int, wmodea);
buf b52(wmodeb_int, wmodeb);

pmos inst1(douta0, doutap0, 0);
pmos inst2(douta1, doutap1, 0);
pmos inst3(douta2, doutap2, 0);
pmos inst4(douta3, doutap3, 0);
pmos inst5(douta4, doutap4, 0);
pmos inst6(douta5, doutap5, 0);
pmos inst7(douta6, doutap6, 0);
pmos inst8(douta7, doutap7, 0);
pmos inst9(douta8, doutap8, 0);

pmos inst10(doutb0, doutbp0, 0);
pmos inst11(doutb1, doutbp1, 0);
pmos inst12(doutb2, doutbp2, 0);
pmos inst13(doutb3, doutbp3, 0);
pmos inst14(doutb4, doutbp4, 0);
pmos inst15(doutb5, doutbp5, 0);
pmos inst16(doutb6, doutbp6, 0);
pmos inst17(doutb7, doutbp7, 0);
pmos inst18(doutb8, doutbp8, 0);

parameter memoryfile = "";
parameter warning_msgs_on = 0; // used to turn off warnings about read &
                               // write to same address at same time.
                               // default = on.  set to 0 to turn them off.

  initial
    begin

      if ( warning_msgs_on == 0 )
        $display("note: ram4k9 warnings disabled. set warning_msgs_on = 1 to enable.");

      if ( memoryfile != "")
        $readmemb ( memoryfile, mem_512_9 );
      else
        begin
          //if ( warning_msgs_on == 1 )
            //$display ( "note: module %m, memory initialization file parameter memoryfile not defined" );
        end
    end


always @(clka_int === 1'bx )
begin
  if ($time > 0) begin
    if (blka_int == 1'b0) begin
      if ( warning_msgs_on == 1 )
        $display("warning : clka went unknown at time %0.1f. instance: %m\n",$realtime);
    end
  end
end

always @(clkb_int === 1'bx )
begin
  if ($time > 0) begin
    if (blkb_int == 1'b0) begin
      if ( warning_msgs_on == 1 )
        $display("warning : clkb went unknown at time %0.1f. instance: %m\n",$realtime);
    end
  end
end

  always @(reset_int )
      begin
      if(reset_int === 1'b0 )
          begin
	  if (pipea_int == 1'b0) begin
	      case (`blka_width_cfg)
	     2'b00 : doutap0 = 1'b0;
	     2'b01 : begin
	         doutap0 = 1'b0;
	         doutap1 = 1'b0;
	     end
	     2'b10 : begin
	         doutap0 = 1'b0;
	         doutap1 = 1'b0;
	         doutap2 = 1'b0;
	         doutap3 = 1'b0;
	     end
	     2'b11 : begin
	         doutap0 = 1'b0;
	         doutap1 = 1'b0;
	         doutap2 = 1'b0;
	         doutap3 = 1'b0;
	         doutap4 = 1'b0;
	         doutap5 = 1'b0;
	         doutap6 = 1'b0;
	         doutap7 = 1'b0;
	         doutap8 = 1'b0;
	     end
	     default:
	         begin
		 //if ( warning_msgs_on == 1 )
		 //    $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9", $time);
	         end
	      endcase
	  end else if (pipea_int == 1'b1) begin
	      case (`blka_width_cfg)
	     2'b00 : begin
	         doutap0      = 1'b0;
	         doutap0_stg1 = 1'b0;
	     end
	     2'b01 : begin
	         doutap0 = 1'b0;
	         doutap1 = 1'b0;
	         doutap0_stg1 = 1'b0;
	         doutap1_stg1 = 1'b0;
	     end
	     2'b10 : begin
	         doutap0= 1'b0;
	         doutap1 = 1'b0;
	         doutap2 = 1'b0;
	         doutap3 = 1'b0;
	         doutap0_stg1= 1'b0;
	         doutap1_stg1 = 1'b0;
	         doutap2_stg1 = 1'b0;
	         doutap3_stg1 = 1'b0;
	     end
	     2'b11 : begin
	         doutap0 = 1'b0;
	         doutap1 = 1'b0;
	         doutap2 = 1'b0;
	         doutap3 = 1'b0;
	         doutap4 = 1'b0;
	         doutap5 = 1'b0;
	         doutap6 = 1'b0;
	         doutap7 = 1'b0;
	         doutap8 = 1'b0;
	         doutap0_stg1 = 1'b0;
	         doutap1_stg1 = 1'b0;
	         doutap2_stg1 = 1'b0;
	         doutap3_stg1 = 1'b0;
	         doutap4_stg1 = 1'b0;
	         doutap5_stg1 = 1'b0;
	         doutap6_stg1 = 1'b0;
	         doutap7_stg1 = 1'b0;
	         doutap8_stg1 = 1'b0;

	     end
	     default:
	         begin
		 //if ( warning_msgs_on == 1 )
		 //    $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9", $time);
	         end
	      endcase
	  end

	  if (pipeb_int == 1'b0) begin
	      case (`blkb_width_cfg)
	     2'b00 : doutbp0 = 1'b0;
	     2'b01 : begin
	         doutbp0 = 1'b0;
	         doutbp1 = 1'b0;
	     end
	     2'b10 : begin
	         doutbp0= 1'b0;
	         doutbp1 = 1'b0;
	         doutbp2 = 1'b0;
	         doutbp3 = 1'b0;
	     end
	     2'b11 : begin
	         doutbp0 = 1'b0;
	         doutbp1 = 1'b0;
	         doutbp2 = 1'b0;
	         doutbp3 = 1'b0;
	         doutbp4 = 1'b0;
	         doutbp5 = 1'b0;
	         doutbp6 = 1'b0;
	         doutbp7 = 1'b0;
	         doutbp8 = 1'b0;
	     end
	     default:
	         begin
		// if ( warning_msgs_on == 1 )
		//     $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9", $time);
	         end
	      endcase

	  end else if (pipeb_int == 1'b1) begin
	      case (`blkb_width_cfg)
	     2'b00 : begin
	         doutbp0 = 1'b0;
	         doutbp0_stg1 = 1'b0;
	     end
	     2'b01 : begin
	         doutbp0 = 1'b0;
	         doutbp1 = 1'b0;
	         doutbp0_stg1 = 1'b0;
	         doutbp1_stg1 = 1'b0;

	     end
	     2'b10 : begin
	         doutbp0= 1'b0;
	         doutbp1 = 1'b0;
	         doutbp2 = 1'b0;
	         doutbp3 = 1'b0;
	         doutbp0_stg1= 1'b0;
	         doutbp1_stg1 = 1'b0;
	         doutbp2_stg1 = 1'b0;
	         doutbp3_stg1 = 1'b0;

	     end
	     2'b11 : begin
	         doutbp0 = 1'b0;
	         doutbp1 = 1'b0;
	         doutbp2 = 1'b0;
	         doutbp3 = 1'b0;
	         doutbp4 = 1'b0;
	         doutbp5 = 1'b0;
	         doutbp6 = 1'b0;
	         doutbp7 = 1'b0;
	         doutbp8 = 1'b0;
	         doutbp0_stg1 = 1'b0;
	         doutbp1_stg1 = 1'b0;
	         doutbp2_stg1 = 1'b0;
	         doutbp3_stg1 = 1'b0;
	         doutbp4_stg1 = 1'b0;
	         doutbp5_stg1 = 1'b0;
	         doutbp6_stg1 = 1'b0;
	         doutbp7_stg1 = 1'b0;
	         doutbp8_stg1 = 1'b0;

	     end
	     default:
	         begin
		 //if ( warning_msgs_on == 1 )
		 //    $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9", $time);
	         end
	      endcase
	  end
          end
      end // reset

// start the ram blka negative edge behavior to handle write/write conflicts
// this part is only added to umc chips since their ram cells are level triggered
// rather than being edge triggered.
always @(negedge clka_int)
begin
    if ((blka_lat == 1'b0) && (reset_int == 1'b1)) begin
	if ( (wena_lat == 1'b0) && addra_valid ) begin  // write mode
	    clka_wr_fe = $time;
	    // check if write from port b and write from port a are to the same address,
	    // and clka and clkb fall simultaneously --> write data is non-deterministic
	    if ( (wenb_lat == 1'b0) && same_addr(addrb, addra, {widthb1,widthb0}, {widtha1,widtha0} ) &&
                                                            ((clkb_wr_fe + tc2cwwl) > clka_wr_fe) ) begin
		$display (" ** warning: port b write and port a write to same address at same time. write data conflict. updating memory contents at conflicting address with x");
		$display (" time: %0.1f ps instance: %m ", $realtime );
		// function call to determine conflicting write data bits based on address and width configuration
		`data_a_reg = drive_data_x (addrb, addra, {widthb1,widthb0}, {widtha1,widtha0}, `data_a_in);

		case (`blka_width_cfg)
		    2'b00 : begin
			    mem_512_9[ addra[11:3] ] [ addra[2:0] ] = dina0_reg;
			end
		    2'b01 : begin
			mem_512_9[ addra[10:2] ] [ addra[1:0] * 2 ] = dina0_reg;
			mem_512_9[ addra[10:2] ] [ addra[1:0] * 2 + 1 ] = dina1_reg;
		    end
		    2'b10 : begin
			mem_512_9[ addra[9:1] ] [ addra[0] * 4 ] = dina0_reg;
			mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 1 ] = dina1_reg;
			mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 2 ] = dina2_reg;
			mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 3 ] = dina3_reg;
		    end
		    2'b11 : begin
			mem_512_9[ addra ] = {dina8_reg,dina7_reg,dina6_reg,dina5_reg,dina4_reg,dina3_reg,dina2_reg,dina1_reg,dina0_reg};
		    end
		    default: begin
			if ( warning_msgs_on == 1 )
			    $display ("warning: invalid width configuration at time %d ns. legal width: 1, 2, 4, 9. instance: %m", $time);
		    end
		endcase
	    end

	    // check if clka falls while there is a write from port b to the same address, x is written to the ram
	    else if ( (wenb_lat == 1'b0) && same_addr(addrb, addra, {widthb1,widthb0}, {widtha1,widtha0} ) &&
				    (clkb_int == 1'b1) ) begin
		if ((clkb_wr_re + tc2cwwh) < clka_wr_re) begin
		    $display (" ** warning: port b write and port a write to same address at same time. write data conflict. updating memory contents at conflicting address with x");
		    $display (" time: %0.1f ps instance: %m ", $realtime );
		    // function call to determine conflicting write data bits based on address and width configuration
		    `data_a_reg = drive_data_x (addrb, addra, {widthb1,widthb0}, {widtha1,widtha0}, `data_a_in);

		    case (`blka_width_cfg)
			2'b00 : begin
				mem_512_9[ addra[11:3] ] [ addra[2:0] ] = dina0_reg;
			    end
			2'b01 : begin
			    mem_512_9[ addra[10:2] ] [ addra[1:0] * 2 ] = dina0_reg;
			    mem_512_9[ addra[10:2] ] [ addra[1:0] * 2 + 1 ] = dina1_reg;
			end
			2'b10 : begin
			    mem_512_9[ addra[9:1] ] [ addra[0] * 4 ] = dina0_reg;
			    mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 1 ] = dina1_reg;
			    mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 2 ] = dina2_reg;
			    mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 3 ] = dina3_reg;
			end
			2'b11 : begin
			    mem_512_9[ addra ] = {dina8_reg,dina7_reg,dina6_reg,dina5_reg,dina4_reg,dina3_reg,dina2_reg,dina1_reg,dina0_reg};
			end
			default: begin
			    if ( warning_msgs_on == 1 )
				$display ("warning: invalid width configuration at time %d ns. legal width: 1, 2, 4, 9. instance: %m", $time);
			end
		    endcase
		end
	    end
	end
    end
end

// start the ram blkb negative edge behavior to handle write/write conflicts
// this part is only added to umc chips since their ram cells are level triggered
// rather than being edge triggered.
always @(negedge clkb_int) begin
    if ((blkb_lat == 1'b0) && (reset_int == 1'b1)) begin
	if ( (wenb_lat == 1'b0) && addrb_valid ) begin // write mode
	    clkb_wr_fe = $time;

	    // check if write from port b and write from port a are to the same address,
	    // and clka and clkb fall simultaneously --> write data is non-deterministic
	    // cases b1, b4
	    if ( (wena_lat == 1'b0) && same_addr(addrb, addra, {widthb1,widthb0}, {widtha1,widtha0} ))
	    begin
		if ((clka_wr_fe + tc2cwwl) > clkb_wr_fe )
		begin
		    $display (" ** warning: port b write and port a write to same address at same time. write data conflict. updating memory contents at conflicting address with x");
		    $display (" time: %0.1f ps instance: %m ", $realtime );
		    // function call to determine conflicting write data bits based on address and width configuration
		    `data_b_reg = drive_data_x (addrb, addra, {widthb1,widthb0}, {widtha1,widtha0}, `data_b_in);

		    case (`blkb_width_cfg)
			2'b00 : begin
				mem_512_9[ addrb[11:3] ] [ addrb[2:0] ] = dinb0_reg;
			    end
			2'b01 : begin
			    mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 ] = dinb0_reg;
			    mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 + 1 ] = dinb1_reg;
			end
			2'b10 : begin
			    mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 ] = dinb0_reg;
			    mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 1 ] = dinb1_reg;
			    mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 2 ] = dinb2_reg;
			    mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 3 ] = dinb3_reg;
			end
			2'b11 : begin
			    mem_512_9[ addrb ] = {dinb8_reg,dinb7_reg,dinb6_reg,dinb5_reg,dinb4_reg,dinb3_reg,dinb2_reg,dinb1_reg,dinb0_reg};
			end
			default: begin
			    if ( warning_msgs_on == 1 )
				$display ("warning: invalid width configuration at time %d ns. legal width: 1, 2, 4, 9. instance: %m", $time);
			end
		    endcase
		end
		// check if clkb falls while there is a write from port a to the same address, data_a_in is written to the ram
		else if ( clka_int == 1'b1 )
		begin
		    // cases b5
		    if ((clka_wr_re + tc2cwwh) < clkb_wr_re) begin
			$display (" ** warning: port b write and port a write to same address at same time. write data conflict. updating memory contents at conflicting address with x");
			$display (" time: %0.1f ps instance: %m ", $realtime );
			// function call to determine conflicting write data bits based on address and width configuration
			`data_b_reg = drive_data_x (addra, addrb, {widtha1,widtha0}, {widthb1,widthb0}, `data_b_in);

			case (`blkb_width_cfg)
			    2'b00 : begin
				    mem_512_9[ addrb[11:3] ] [ addrb[2:0] ] = dinb0_reg;
				end
			    2'b01 : begin
				mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 ] = dinb0_reg;
				mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 + 1 ] = dinb1_reg;
			    end
			    2'b10 : begin
				mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 ] = dinb0_reg;
				mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 1 ] = dinb1_reg;
				mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 2 ] = dinb2_reg;
				mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 3 ] = dinb3_reg;
			    end
			    2'b11 : begin
				mem_512_9[ addrb ] = {dinb8_reg,dinb7_reg,dinb6_reg,dinb5_reg,dinb4_reg,dinb3_reg,dinb2_reg,dinb1_reg,dinb0_reg};
			    end
			    default: begin
				if ( warning_msgs_on == 1 )
				    $display ("warning: invalid width configuration at time %d ns. legal width: 1, 2, 4, 9. instance: %m", $time);
			    end
			endcase
		    end
		end
	    end
	end
    end
end

// start the ram blka write/read  behavior section
always @(posedge clka_int) begin
  blka_lat = blka_int;

  if (pipea_int == 1'b1) begin
    case (`blka_width_cfg)
      2'b00 : begin
               doutap0 = doutap0_stg1;
              end
      2'b01 : begin
               doutap0 = doutap0_stg1;
               doutap1 = doutap1_stg1;
              end
      2'b10 : begin
               doutap0 = doutap0_stg1;
               doutap1 = doutap1_stg1;
               doutap2 = doutap2_stg1;
               doutap3 = doutap3_stg1;
              end
      2'b11 : begin
               doutap0 = doutap0_stg1;
               doutap1 = doutap1_stg1;
               doutap2 = doutap2_stg1;
               doutap3 = doutap3_stg1;
               doutap4 = doutap4_stg1;
               doutap5 = doutap5_stg1;
               doutap6 = doutap6_stg1;
               doutap7 = doutap7_stg1;
               doutap8 = doutap8_stg1;
             end
      default:
              begin
                if ( warning_msgs_on == 1 )
                  $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9. instance: %m", $time);
              end
    endcase
  end
  else if (pipea_int == 1'bx) begin
    if ( warning_msgs_on == 1 )
      $display ("warning: pipea unknown at time %d ns, no data was read. instance: %m", $time);
    doutap0 = 1'bx;
    doutap1 = 1'bx;
    doutap2 = 1'bx;
    doutap3 = 1'bx;
    doutap4 = 1'bx;
    doutap5 = 1'bx;
    doutap6 = 1'bx;
    doutap7 = 1'bx;
    doutap8 = 1'bx;
  end

  if ((blka_int == 1'b0) && (reset_int == 1'b1)) begin

    wena_lat = wena_int;

    addra = get_address(`blka_addr);   // get the address (read or write )
    addra_valid = 1;

    if ((^addra) === 1'bx) begin
      addra_valid = 0;
      if ( warning_msgs_on == 1 )
        $display(" warning: illegal address on port a at time %0.1f! instance: %m", $realtime);
    end
    else if ((`blka_width_cfg == 2'b00) && (addra > 4095)) begin
      addra_valid = 0;
      if ( warning_msgs_on == 1 )
        $display(" warning: illegal address on port a at time %0.1f! instance: %m", $realtime);
    end
    else if ((`blka_width_cfg == 2'b01) && (addra > 2047)) begin
      addra_valid = 0;
      if ( warning_msgs_on == 1 )
        $display(" warning: illegal address on port a at time %0.1f! instance: %m", $realtime);
    end
    else if ((`blka_width_cfg == 2'b10) && (addra > 1023)) begin
      addra_valid = 0;
      if ( warning_msgs_on == 1 )
        $display(" warning: illegal address on port a at time %0.1f! instance: %m", $realtime);
    end
    else if ((`blka_width_cfg == 2'b11) && (addra > 511)) begin
      addra_valid = 0;
      if ( warning_msgs_on == 1 )
        $display(" warning: illegal address on port a at time %0.1f! instance: %m", $realtime);
    end

	if ( (wena_int == 1'b0) && addra_valid ) begin  // write mode
	clka_wr_re = $time;

	    // assign write data to data registers for writing into the memory array
	    `data_a_reg = `data_a_in;
	    // assign write data to bypass registers for driving onto rd if mode=1
	    `data_a_byp = `data_a_in;

      // check if write from port b and write from port a are to the same address, write data is non-deterministic
	    // cases b1-b3
      if ( (wenb_lat == 1'b0) && same_addr(addrb, addra, {widthb1,widthb0}, {widtha1,widtha0} ) &&
                                                            ((clkb_wr_re + tc2cwwh) > clka_wr_re) ) begin
        $display (" ** warning: port b write and port a write to same address at same time. write data conflict. updating memory contents at conflicting address with x");
        $display (" time: %0.1f ps instance: %m ", $realtime );
        // function call to determine conflicting write data bits based on address and width configuration
        `data_a_reg = drive_data_x (addrb, addra, {widthb1,widthb0}, {widtha1,widtha0}, `data_a_in);
      end

      // check for read from port b and write from port a to the same address, read data on port b is driven to x
      if ( (wenb_lat == 1'b1) && same_addr(addrb, addra, {widthb1,widthb0}, {widtha1,widtha0}) &&
                                                              ((clkb_rd_re + tc2crwh) > clka_wr_re) ) begin
        $display (" ** warning: port b read and port a write to same address at same time. port b read data is unpredictable, driving read data to x");
        $display (" time: %0.1f ps instance: %m ", $realtime );

        if (pipeb_int == 1'b1)
		begin
		    case (`blkb_width_cfg)
			2'b00 : begin
					doutbp0_stg1 = mem_512_9[ addrb[11:3] ] [ addrb[2:0] ];
					end
			2'b01 : begin
					doutbp0_stg1 = mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 ];
					doutbp1_stg1 = mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 + 1 ];
					end
			2'b10 : begin
					doutbp0_stg1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 ];
					doutbp1_stg1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 1 ];
					doutbp2_stg1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 2 ];
					doutbp3_stg1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 3 ];
					end
			2'b11 : begin
					{doutbp8_stg1, doutbp7_stg1, doutbp6_stg1, doutbp5_stg1, doutbp4_stg1,
								     doutbp3_stg1, doutbp2_stg1, doutbp1_stg1, doutbp0_stg1} = mem_512_9[ addrb ];
					end
			default:
			     begin
				if ( warning_msgs_on == 1 )
				    $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9. instance: %m", $time);
			     end
		    endcase
          `datap_b_out = drive_data_x (addra, addrb, {widtha1,widtha0}, {widthb1,widthb0}, `datap_b_out);
		end
        else if (pipeb_int == 1'b0)
		begin
		    case (`blkb_width_cfg)
			2'b00 : begin
					doutbp0 = mem_512_9[ addrb[11:3] ] [ addrb[2:0] ];
					end
			2'b01 : begin
					doutbp0 = mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 ];
					doutbp1 = mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 + 1 ];
					end
			2'b10 : begin
					doutbp0 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 ];
					doutbp1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 1 ];
					doutbp2 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 2 ];
					doutbp3 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 3 ];
					end
			2'b11 : begin
					{doutbp8, doutbp7, doutbp6, doutbp5, doutbp4,
							    doutbp3, doutbp2, doutbp1, doutbp0} = mem_512_9[ addrb ];
					end
			default:
			     begin
				if ( warning_msgs_on == 1 )
				    $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9. instance: %m", $time);
			     end
		    endcase
          `data_b_out = drive_data_x (addra, addrb, {widtha1,widtha0}, {widthb1,widthb0}, `data_b_out);
		end
      end


      case (`blka_width_cfg)
		2'b00 : begin
			mem_512_9[ addra[11:3] ] [ addra[2:0] ] = dina0_reg;
			    if (wmodea_int == 1'b1) begin
				if (pipea_int == 1'b0) begin
				    doutap0 = dina0_bypass;
				end else if (pipea_int == 1'b1) begin
				    doutap0_stg1 = dina0_bypass;
				end
			    end
		    end

		2'b01 : begin
		    mem_512_9[ addra[10:2] ] [ addra[1:0] * 2 ] = dina0_reg;
		    mem_512_9[ addra[10:2] ] [ addra[1:0] * 2 + 1 ] = dina1_reg;
		    if (wmodea_int == 1'b1) begin
			if (pipea_int == 1'b0) begin
			    doutap0 = dina0_bypass;
			    doutap1 = dina1_bypass;
			end else if (pipea_int == 1'b1) begin
			    doutap0_stg1 = dina0_bypass;
			    doutap1_stg1 = dina1_bypass;
			end
		    end
		end

		2'b10 : begin
		    mem_512_9[ addra[9:1] ] [ addra[0] * 4 ] = dina0_reg;
		    mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 1 ] = dina1_reg;
		    mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 2 ] = dina2_reg;
		    mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 3 ] = dina3_reg;

		    if (wmodea_int == 1'b1) begin
			if (pipea_int == 1'b0) begin
			    doutap0 = dina0_bypass;
			    doutap1 = dina1_bypass;
			    doutap2 = dina2_bypass;
			    doutap3 = dina3_bypass;
			end else if (pipea_int == 1'b1) begin
			    doutap0_stg1 = dina0_bypass;
			    doutap1_stg1 = dina1_bypass;
			    doutap2_stg1 = dina2_bypass;
			    doutap3_stg1 = dina3_bypass;
			end
		    end
		end

		2'b11 : begin
		    mem_512_9[ addra ] = {dina8_reg,dina7_reg,dina6_reg,dina5_reg,dina4_reg,dina3_reg,dina2_reg,dina1_reg,dina0_reg};
		    if (wmodea_int == 1'b1) begin
			if (pipea_int == 1'b0) begin
			    doutap0 = dina0_bypass;
			    doutap1 = dina1_bypass;
			    doutap2 = dina2_bypass;
			    doutap3 = dina3_bypass;
			    doutap4 = dina4_bypass;
			    doutap5 = dina5_bypass;
			    doutap6 = dina6_bypass;
			    doutap7 = dina7_bypass;
			    doutap8 = dina8_bypass;
			end else if (pipea_int == 1'b1) begin
			    doutap0_stg1 = dina0_bypass;
			    doutap1_stg1 = dina1_bypass;
			    doutap2_stg1 = dina2_bypass;
			    doutap3_stg1 = dina3_bypass;
			    doutap4_stg1 = dina4_bypass;
			    doutap5_stg1 = dina5_bypass;
			    doutap6_stg1 = dina6_bypass;
			    doutap7_stg1 = dina7_bypass;
			    doutap8_stg1 = dina8_bypass;
			end
		    end
		end

		default: begin
		    if ( warning_msgs_on == 1 )
			$display ("warning: invalid width configuration at time %d ns. legal width: 1, 2, 4, 9. instance: %m", $time);
		end
	    endcase

	    end else if ( (wena_int == 1'b1) && addra_valid ) begin // read mode

        clka_rd_re = $time;

		// no pipeline
        if (pipea_int == 1'b0) begin
		    case (`blka_width_cfg)
			    2'b00 : begin
					    doutap0 = mem_512_9[ addra[11:3] ] [ addra[2:0] ];
					    end
			    2'b01 : begin
					    doutap0 = mem_512_9[ addra[10:2] ] [ addra[1:0] * 2 ];
					    doutap1 = mem_512_9[ addra[10:2] ] [ addra[1:0] * 2 + 1 ];
				    end
			    2'b10 : begin
					    doutap0 = mem_512_9[ addra[9:1] ] [ addra[0] * 4 ];
					    doutap1 = mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 1 ];
					    doutap2 = mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 2 ];
					    doutap3 = mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 3 ];
				    end
			    2'b11 : begin
					    {doutap8, doutap7, doutap6, doutap5, doutap4,
								doutap3, doutap2, doutap1, doutap0} = mem_512_9[ addra ];
					    end
			     default:
				 begin
				    if ( warning_msgs_on == 1 )
					$display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9. instance: %m", $time);
				 end
		    endcase
		    // check for write from port b and read from port a to the same address, read data on port a is driven to x
          if ( (wenb_lat == 1'b0) && same_addr(addrb, addra, {widthb1,widthb0}, {widtha1,widtha0}) &&
                                                                  ((clkb_wr_re + tc2cwrh) > clka_rd_re) ) begin
            $display (" ** warning: port b write and port a read to same address at same time. port a read data is unpredictable, driving read data to x.");
            $display (" time: %0.1f ps instance: %m ", $realtime );
            `data_a_out = drive_data_x (addrb, addra, {widthb1,widthb0}, {widtha1,widtha0}, `data_a_out);
          end

        end else if (pipea_int == 1'b1) begin		// pipeline
		    case (`blka_width_cfg)
			2'b00 : begin
					doutap0_stg1 = mem_512_9[ addra[11:3] ] [ addra[2:0] ];
					end
			2'b01 : begin
					doutap0_stg1 = mem_512_9[ addra[10:2] ] [ addra[1:0] * 2 ];
					doutap1_stg1 = mem_512_9[ addra[10:2] ] [ addra[1:0] * 2 + 1 ];
					end
			2'b10 : begin
					doutap0_stg1 = mem_512_9[ addra[9:1] ] [ addra[0] * 4 ];
					doutap1_stg1 = mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 1 ];
					doutap2_stg1 = mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 2 ];
					doutap3_stg1 = mem_512_9[ addra[9:1] ] [ addra[0] * 4 + 3 ];
					end
			2'b11 : begin
					{doutap8_stg1, doutap7_stg1, doutap6_stg1, doutap5_stg1, doutap4_stg1,
								     doutap3_stg1, doutap2_stg1, doutap1_stg1, doutap0_stg1} = mem_512_9[ addra ];
					end
			 default:
			     begin
				if ( warning_msgs_on == 1 )
				    $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9. instance: %m", $time);
			     end
		    endcase
          // check for write from port b and read from port a to the same address, read data on port a is driven to x
		    if ( (wenb_lat == 1'b0) && same_addr(addrb, addra, {widthb1,widthb0}, {widtha1,widtha0}) &&
																((clkb_wr_re + tc2cwrh) > clka_rd_re) ) begin
			$display (" ** warning: port b write and port a read to same address at same time. port a read data is unpredictable, driving read data to x.");
			$display (" time: %0.1f ps instance: %m ", $realtime );
			`datap_a_out = drive_data_x (addrb, addra, {widthb1,widthb0}, {widtha1,widtha0}, `datap_a_out);
		    end

        end else begin
                if ( warning_msgs_on == 1 )
                  $display ("warning: pipea unknown at time %d ns, no data was read. instance: %m", $time);
                doutap0 = 1'bx;
                doutap1 = 1'bx;
                doutap2 = 1'bx;
                doutap3 = 1'bx;
                doutap4 = 1'bx;
                doutap5 = 1'bx;
                doutap6 = 1'bx;
                doutap7 = 1'bx;
                doutap8 = 1'bx;
              end
      end else if ( (wena_int == 1'b0) && (addra_valid == 0) ) begin
        if ( warning_msgs_on == 1 )
          $display("illegal write address on port a, write not initiated. instance: %m");
      end else if ( (wena_int == 1'b1) && (addra_valid == 0) ) begin
        if ( warning_msgs_on == 1 )
          $display("illegal read address on port a, read not initiated. instance: %m");
      end else begin
        if ( warning_msgs_on == 1 )
          $display("warning: wenais unknown at time %d ns. instance: %m", $time);
      end
    end
 end



// start the ram blkb write/read  behavior section

always @(posedge clkb_int) begin

  blkb_lat = blkb_int;

  if (pipeb_int == 1'b1) begin
    case (`blkb_width_cfg)
      2'b00 : begin
               doutbp0 = doutbp0_stg1;
              end
      2'b01 : begin
               doutbp0 = doutbp0_stg1;
               doutbp1 = doutbp1_stg1;
              end
      2'b10 : begin
               doutbp0 = doutbp0_stg1;
               doutbp1 = doutbp1_stg1;
               doutbp2 = doutbp2_stg1;
               doutbp3 = doutbp3_stg1;
              end
      2'b11 : begin
               doutbp0 = doutbp0_stg1;
               doutbp1 = doutbp1_stg1;
               doutbp2 = doutbp2_stg1;
               doutbp3 = doutbp3_stg1;
               doutbp4 = doutbp4_stg1;
               doutbp5 = doutbp5_stg1;
               doutbp6 = doutbp6_stg1;
               doutbp7 = doutbp7_stg1;
               doutbp8 = doutbp8_stg1;
             end
      default:
              begin
                if ( warning_msgs_on == 1 )
                  $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9. instance: %m", $time);
              end
    endcase
  end
  else if (pipeb_int == 1'bx) begin
    if ( warning_msgs_on == 1 )
      $display ("warning: pipeb unknown at time %d ns, no data was read. instance: %m", $time);
    doutbp0 = 1'bx;
    doutbp1 = 1'bx;
    doutbp2 = 1'bx;
    doutbp3 = 1'bx;
    doutbp4 = 1'bx;
    doutbp5 = 1'bx;
    doutbp6 = 1'bx;
    doutbp7 = 1'bx;
    doutbp8 = 1'bx;
  end


  if ((blkb_int == 1'b0) && (reset_int == 1'b1)) begin

    wenb_lat = wenb_int;

    addrb = get_address(`blkb_addr);   // get the address (read or write )
    addrb_valid = 1;

    if ((^addrb) === 1'bx) begin
      addrb_valid = 0;
      if ( warning_msgs_on == 1 )
        $display(" warning: illegal address on port b at time %0.1f! instance: %m", $realtime);
    end
    else if ((`blkb_width_cfg == 2'b00) && (addrb > 4095)) begin
      addrb_valid = 0;
      if ( warning_msgs_on == 1 )
        $display(" warning: illegal address on port b at time %0.1f! instance: %m", $realtime);
    end
    else if ((`blkb_width_cfg == 2'b01) && (addrb > 2047)) begin
      addrb_valid = 0;
      if ( warning_msgs_on == 1 )
        $display(" warning: illegal address on port b at time %0.1f! instance: %m", $realtime);
    end
    else if ((`blkb_width_cfg == 2'b10) && (addrb > 1023)) begin
      addrb_valid = 0;
      if ( warning_msgs_on == 1 )
        $display(" warning: illegal address on port b at time %0.1f! instance: %m", $realtime);
    end
    else if ((`blkb_width_cfg == 2'b11) && (addrb > 511)) begin
      addrb_valid = 0;
      if ( warning_msgs_on == 1 )
        $display(" warning: illegal address on port b at time %0.1f! instance: %m", $realtime);
    end

	if ( (wenb_int == 1'b0) && addrb_valid ) begin // write mode
	    clkb_wr_re = $time;

	    // assign write data to data registers for writing into the memory array
	    `data_b_reg = `data_b_in;
	    // assign write data to bypass registers for driving onto rd if mode=1
	    `data_b_byp = `data_b_in;

      // check if write from port a and write from port a are to the same address, write data is non-deterministic
	    // cases b1-b3
      if ( (wena_lat == 1'b0) && same_addr(addra, addrb, {widtha1,widtha0}, {widthb1,widthb0} ) &&
                                                            ((clka_wr_re + tc2cwwh) > clkb_wr_re) ) begin
        $display (" ** warning: port a write and port b write to same address at same time. write data conflict. updating memory contents at conflicting address with x");
        // function call to determine conflicting write data bits based on address and width configuration
        $display (" time: %0.1f ps instance: %m ", $realtime );
        `data_b_reg = drive_data_x (addra, addrb, {widtha1,widtha0}, {widthb1,widthb0}, `data_b_in);
	    end

      // check for read from port a and write from port b to the same address, read data on port a is driven to x
      if ( (wena_lat == 1'b1) && same_addr(addra, addrb, {widtha1,widtha0}, {widthb1,widthb0}) &&
                                                              ((clka_rd_re + tc2crwh) > clkb_wr_re) ) begin
        $display (" ** warning: port a read and port b write to same address at same time. port a read data is unpredictable, driving read data to x");
        $display (" time: %0.1f ps instance: %m ", $realtime );

        if (pipea_int == 1'b1)
		begin
		    case (`blkb_width_cfg)
			2'b00 : begin
					doutbp0_stg1 = mem_512_9[ addrb[11:3] ] [ addrb[2:0] ];
					end
			2'b01 : begin
					doutbp0_stg1 = mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 ];
					doutbp1_stg1 = mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 + 1 ];
					end
			2'b10 : begin
					doutbp0_stg1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 ];
					doutbp1_stg1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 1 ];
					doutbp2_stg1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 2 ];
					doutbp3_stg1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 3 ];
					end
			2'b11 : begin
					{doutbp8_stg1, doutbp7_stg1, doutbp6_stg1, doutbp5_stg1, doutbp4_stg1,
								     doutbp3_stg1, doutbp2_stg1, doutbp1_stg1, doutbp0_stg1} = mem_512_9[ addrb ];
					end
			default:
			     begin
				if ( warning_msgs_on == 1 )
				    $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9. instance: %m", $time);
			     end
		    endcase
          `datap_a_out = drive_data_x (addrb, addra, {widthb1,widthb0}, {widtha1,widtha0}, `datap_a_out);
		end
        else if (pipea_int == 1'b0)
		begin
          case (`blkb_width_cfg)
			2'b00 : begin
					doutbp0 = mem_512_9[ addrb[11:3] ] [ addrb[2:0] ];
					end
			2'b01 : begin
					doutbp0 = mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 ];
					doutbp1 = mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 + 1 ];
					end
			2'b10 : begin
					doutbp0 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 ];
					doutbp1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 1 ];
					doutbp2 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 2 ];
					doutbp3 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 3 ];
					end
			2'b11 : begin
					{doutbp8, doutbp7, doutbp6, doutbp5, doutbp4,
							    doutbp3, doutbp2, doutbp1, doutbp0} = mem_512_9[ addrb ];
					end
			default:
			     begin
				if ( warning_msgs_on == 1 )
				    $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9. instance: %m", $time);
			     end
		    endcase
		    `data_a_out = drive_data_x (addrb, addra, {widthb1,widthb0}, {widtha1,widtha0}, `data_a_out);
		end
      end


	    case (`blkb_width_cfg)
		2'b00 : begin
			mem_512_9[ addrb[11:3] ] [ addrb[2:0] ] = dinb0_reg;
			    if (wmodeb_int == 1'b1) begin
				if (pipeb_int == 1'b0) begin
				    doutbp0 = dinb0_bypass;
				end else if (pipeb_int == 1'b1) begin
				    doutbp0_stg1 = dinb0_bypass;
				end
			    end
		    end

		2'b01 : begin
		    mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 ] = dinb0_reg;
		    mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 + 1 ] = dinb1_reg;
		    if (wmodeb_int == 1'b1) begin
			if (pipeb_int == 1'b0) begin
			    doutbp0 = dinb0_bypass;
			    doutbp1 = dinb1_bypass;
			end else if (pipeb_int == 1'b1) begin
			    doutbp0_stg1 = dinb0_bypass;
			    doutbp1_stg1 = dinb1_bypass;
			end
		    end
		end

		2'b10 : begin
		    mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 ] = dinb0_reg;
		    mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 1 ] = dinb1_reg;
		    mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 2 ] = dinb2_reg;
		    mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 3 ] = dinb3_reg;

		    if (wmodeb_int == 1'b1) begin
			if (pipeb_int == 1'b0) begin
			    doutbp0 = dinb0_bypass;
			    doutbp1 = dinb1_bypass;
			    doutbp2 = dinb2_bypass;
			    doutbp3 = dinb3_bypass;
			end else if (pipeb_int == 1'b1) begin
			    doutbp0_stg1 = dinb0_bypass;
			    doutbp1_stg1 = dinb1_bypass;
			    doutbp2_stg1 = dinb2_bypass;
			    doutbp3_stg1 = dinb3_bypass;
			end
		    end
		end

		2'b11 : begin
		    mem_512_9[ addrb ] = {dinb8_reg,dinb7_reg,dinb6_reg,dinb5_reg,dinb4_reg,dinb3_reg,dinb2_reg,dinb1_reg,dinb0_reg};
		    if (wmodeb_int == 1'b1) begin
			if (pipeb_int == 1'b0) begin
			    doutbp0 = dinb0_bypass;
			    doutbp1 = dinb1_bypass;
			    doutbp2 = dinb2_bypass;
			    doutbp3 = dinb3_bypass;
			    doutbp4 = dinb4_bypass;
			    doutbp5 = dinb5_bypass;
			    doutbp6 = dinb6_bypass;
			    doutbp7 = dinb7_bypass;
			    doutbp8 = dinb8_bypass;
			end else if (pipeb_int == 1'b1) begin
			    doutbp0_stg1 = dinb0_bypass;
			    doutbp1_stg1 = dinb1_bypass;
			    doutbp2_stg1 = dinb2_bypass;
			    doutbp3_stg1 = dinb3_bypass;
			    doutbp4_stg1 = dinb4_bypass;
			    doutbp5_stg1 = dinb5_bypass;
			    doutbp6_stg1 = dinb6_bypass;
			    doutbp7_stg1 = dinb7_bypass;
			    doutbp8_stg1 = dinb8_bypass;
			end
		    end
		end

		default: begin
		    if ( warning_msgs_on == 1 )
			$display ("warning: invalid width configuration at time %d ns. legal width: 1, 2, 4, 9. instance: %m", $time);
		end
endcase


   end else if ( (wenb_int == 1'b1) && addrb_valid ) begin		// read mode
        clkb_rd_re = $time;

        if (pipeb_int == 1'b0) begin 	// no pipeline
		    case (`blkb_width_cfg)
			2'b00 : begin
					doutbp0 = mem_512_9[ addrb[11:3] ] [ addrb[2:0] ];
					end
			2'b01 : begin
					doutbp0 = mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 ];
					doutbp1 = mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 + 1 ];
					end
			2'b10 : begin
					doutbp0 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 ];
					doutbp1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 1 ];
					doutbp2 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 2 ];
					doutbp3 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 3 ];
					end
			2'b11 : begin
					{doutbp8, doutbp7, doutbp6, doutbp5, doutbp4,
							    doutbp3, doutbp2, doutbp1, doutbp0} = mem_512_9[ addrb ];
					end
			default:
			     begin
				if ( warning_msgs_on == 1 )
				    $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9. instance: %m", $time);
			     end
		    endcase
		    // check for write from port a and read from port b to the same address, read data on port b is driven to x
		    if ( (wena_lat == 1'b0) && same_addr(addra, addrb, {widtha1,widtha0}, {widthb1,widthb0}) &&
                                                                  ((clka_wr_re + tc2cwrh) > clkb_rd_re) ) begin
			$display (" ** warning: port a write and port b read to same address at same time. port b read data is unpredictable, driving read data to x.");
			$display (" time: %0.1f ps instance: %m ", $realtime );
			`data_b_out = drive_data_x (addra, addrb, {widtha1,widtha0}, {widthb1,widthb0}, `data_b_out);
		    end

        end else if (pipeb_int == 1'b1) begin
		    case (`blkb_width_cfg)
			2'b00 : begin
					doutbp0_stg1 = mem_512_9[ addrb[11:3] ] [ addrb[2:0] ];
					end
			2'b01 : begin
					doutbp0_stg1 = mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 ];
					doutbp1_stg1 = mem_512_9[ addrb[10:2] ] [ addrb[1:0] * 2 + 1 ];
					end
			2'b10 : begin
					doutbp0_stg1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 ];
					doutbp1_stg1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 1 ];
					doutbp2_stg1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 2 ];
					doutbp3_stg1 = mem_512_9[ addrb[9:1] ] [ addrb[0] * 4 + 3 ];
					end
			2'b11 : begin
					{doutbp8_stg1, doutbp7_stg1, doutbp6_stg1, doutbp5_stg1, doutbp4_stg1,
								     doutbp3_stg1, doutbp2_stg1, doutbp1_stg1, doutbp0_stg1} = mem_512_9[ addrb ];
					end
			default:
			     begin
				if ( warning_msgs_on == 1 )
				    $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9. instance: %m", $time);
			     end
		    endcase
          // check for write from port a and read from port b to the same address, read data on port b is driven to x
          if ( (wena_lat == 1'b0) && same_addr(addra, addrb, {widtha1,widtha0}, {widthb1,widthb0}) &&
                                                                  ((clka_wr_re + tc2cwrh) > clkb_rd_re) ) begin
            $display (" ** warning: port a write and port b read to same address at same time. port b read data is unpredictable, driving read data to x.");
            $display (" time: %0.1f ps instance: %m ", $realtime );
            `datap_b_out = drive_data_x (addra, addrb, {widtha1,widtha0}, {widthb1,widthb0}, `datap_b_out);
          end

       end else begin
                   if ( warning_msgs_on == 1 )
                     $display ("warning: pipeb unknown at time %d ns, no data was read. instance: %m", $time);
                   doutbp0 = 1'bx;
                   doutbp1 = 1'bx;
                   doutbp2 = 1'bx;
                   doutbp3 = 1'bx;
                   doutbp4 = 1'bx;
                   doutbp5 = 1'bx;
                   doutbp6 = 1'bx;
                   doutbp7 = 1'bx;
                   doutbp8 = 1'bx;
                end
      end else if ( (wenb_int == 1'b0) && (addrb_valid == 0) ) begin
        if ( warning_msgs_on == 1 )
          $display("illegal write address on port b, write not initiated. instance: %m");
      end else if ( (wenb_int == 1'b1) && (addrb_valid == 0) ) begin
        if ( warning_msgs_on == 1 )
          $display("illegal read address on port b, read not initiated. instance: %m");
      end else begin
        if ( warning_msgs_on == 1 )
          $display("warning: wenb is unknown at time %d ns. instance: %m", $time);
      end
    end
 end

 // function to drive read data bus to "x" depending on width configuration
 // function to check if write and read operations are accessing the same memory location

 function same_addr;
   input integer waddr, raddr;
   input [1:0]   ww, rw;
   integer       wr_addr, rd_addr;
   begin
     same_addr = 1'b0;
     if ( ww > rw ) begin
       rd_addr = raddr >> (  ww - rw );
       wr_addr = waddr;
     end
     else if ( rw > ww )begin
       rd_addr = raddr;
       wr_addr = waddr >> (  rw - ww );
     end
     else begin
       rd_addr = raddr;
       wr_addr = waddr;
     end
     if ( wr_addr == rd_addr ) begin
       same_addr = 1'b1;
     end
   end
 endfunction


 // function to drive read data bus to "x" depending on width configuration
 function [8:0] drive_data_x;
   input integer waddr, raddr;
   input [1:0]   ww, rw;
   input [8:0]   rd_data;
   integer       index, i;
   begin
     drive_data_x = rd_data;
     case(rw)
       2'b00 : begin
                 drive_data_x [ 0 ] = 1'bx;
               end
       2'b01 : begin
                 if ( ww == 2'b00 )
                   drive_data_x [ waddr[0] ] = 1'bx;
                 else
                   drive_data_x [ 1:0 ] = 2'bx;
               end
       2'b10 : begin
                 if ( ww == 2'b00 )
                   drive_data_x [ waddr[1:0] ] = 1'bx;
                 else if ( ww == 2'b01 ) begin
                   index = waddr[0] * 2;
                   for ( i=index; i<index+2; i=i+1 )
                     drive_data_x [ i ] = 1'bx;
                 end else
                   drive_data_x [ 3:0 ] = 4'bx;
               end
       2'b11 : begin
                 if ( ww == 2'b00 )
                   drive_data_x [ waddr[2:0] ] = 1'bx;
                 else if ( ww == 2'b01 ) begin
                   index = waddr[1:0] * 2;
                   for ( i=index; i<index+2; i=i+1 )
                     drive_data_x [ i ] = 1'bx;
                 end else if ( ww == 2'b10 ) begin
                   index = waddr[0] * 4;
                   for ( i=index; i<index+4; i=i+1 )
                     drive_data_x [ i ] = 1'bx;
                 end else
                   drive_data_x [ 8:0 ] = 9'bx;
               end
       default: begin
                  $display ("warning: invalid width configuration at time %d ns, legal width: 1,2,4,9. instance: %m", $time);
                end
      endcase
   end

 endfunction

 // function to convert addr vector to integer

 function integer get_address;
   input [11:0] addr_signal;
   integer      addr;
   begin
     // the address calculation is based on  width,  because we assume that
     // users (or actgen) will connect low unused address pin to gnd (1'b0), otherwise it may cause problem !
     addr =  addr_signal[11]*2048 + addr_signal[10]*1024 + addr_signal[9]*512 + addr_signal[8]*256 +
             addr_signal[7]*128 + addr_signal[6]*64 + addr_signal[5]*32 + addr_signal[4]*16 +
             addr_signal[3]*8 + addr_signal[2]*4 + addr_signal[1]*2 + addr_signal[0]*1;
     get_address = addr;
   end
 endfunction


   specify

      specparam   libname     = "proasic3";
      (posedge clka => (douta0+:douta0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clka => (douta1+:douta1) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clka => (douta2+:douta2) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clka => (douta3+:douta3) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clka => (douta4+:douta4) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clka => (douta5+:douta5) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clka => (douta6+:douta6) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clka => (douta7+:douta7) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clka => (douta8+:douta8) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);

      (posedge clkb => (doutb0+:doutb0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clkb => (doutb1+:doutb1) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clkb => (doutb2+:doutb2) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clkb => (doutb3+:doutb3) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clkb => (doutb4+:doutb4) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clkb => (doutb5+:doutb5) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clkb => (doutb6+:doutb6) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clkb => (doutb7+:doutb7) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (posedge clkb => (doutb8+:doutb8) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);


      (negedge reset => (douta0+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (douta1+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (douta2+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (douta3+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (douta4+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (douta5+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (douta6+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (douta7+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (douta8+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);

      (negedge reset => (doutb0+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (doutb1+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (doutb2+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (doutb3+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (doutb4+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (doutb5+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (doutb6+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (doutb7+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);
      (negedge reset => (doutb8+:1'b0) ) = (1.0:1.0:1.0, 1.0:1.0:1.0);


      $width(posedge clka, 0.0, 0, notify_reg);
      $width(negedge clka, 0.0, 0, notify_reg);
      $width(posedge clkb, 0.0, 0, notify_reg);
      $width(negedge clkb, 0.0, 0, notify_reg);


      $setup(posedge addra11, posedge clka &&& blka_en, 0.0, notify_reg);
      $setup(negedge addra11, posedge clka &&& blka_en, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, posedge addra11, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, negedge addra11, 0.0, notify_reg);
      $setup(posedge addra10, posedge clka &&& blka_en, 0.0, notify_reg);
      $setup(negedge addra10, posedge clka &&& blka_en, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, posedge addra10, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, negedge addra10, 0.0, notify_reg);
      $setup(posedge addra9, posedge clka &&& blka_en, 0.0, notify_reg);
      $setup(negedge addra9, posedge clka &&& blka_en, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, posedge addra9, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, negedge addra9, 0.0, notify_reg);
      $setup(posedge addra8, posedge clka &&& blka_en, 0.0, notify_reg);
      $setup(negedge addra8, posedge clka &&& blka_en, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, posedge addra8, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, negedge addra8, 0.0, notify_reg);
      $setup(posedge addra7, posedge clka &&& blka_en, 0.0, notify_reg);
      $setup(negedge addra7, posedge clka &&& blka_en, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, posedge addra7, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, negedge addra7, 0.0, notify_reg);
      $setup(posedge addra6, posedge clka &&& blka_en, 0.0, notify_reg);
      $setup(negedge addra6, posedge clka &&& blka_en, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, posedge addra6, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, negedge addra6, 0.0, notify_reg);
      $setup(posedge addra5, posedge clka &&& blka_en, 0.0, notify_reg);
      $setup(negedge addra5, posedge clka &&& blka_en, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, posedge addra5, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, negedge addra5, 0.0, notify_reg);
      $setup(posedge addra4, posedge clka &&& blka_en, 0.0, notify_reg);
      $setup(negedge addra4, posedge clka &&& blka_en, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, posedge addra4, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, negedge addra4, 0.0, notify_reg);
      $setup(posedge addra3, posedge clka &&& blka_en, 0.0, notify_reg);
      $setup(negedge addra3, posedge clka &&& blka_en, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, posedge addra3, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, negedge addra3, 0.0, notify_reg);
      $setup(posedge addra2, posedge clka &&& blka_en, 0.0, notify_reg);
      $setup(negedge addra2, posedge clka &&& blka_en, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, posedge addra2, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, negedge addra2, 0.0, notify_reg);
      $setup(posedge addra1, posedge clka &&& blka_en, 0.0, notify_reg);
      $setup(negedge addra1, posedge clka &&& blka_en, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, posedge addra1, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, negedge addra1, 0.0, notify_reg);
      $setup(posedge addra0, posedge clka &&& blka_en, 0.0, notify_reg);
      $setup(negedge addra0, posedge clka &&& blka_en, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, posedge addra0, 0.0, notify_reg);
      $hold(posedge clka &&& blka_en, negedge addra0, 0.0, notify_reg);

      $setup(posedge dina8, posedge clka &&& blka_wen, 0.0, notify_reg);
      $setup(negedge dina8, posedge clka &&& blka_wen, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, posedge dina8, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, negedge dina8, 0.0, notify_reg);
      $setup(posedge dina7, posedge clka &&& blka_wen, 0.0, notify_reg);
      $setup(negedge dina7, posedge clka &&& blka_wen, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, posedge dina7, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, negedge dina7, 0.0, notify_reg);
      $setup(posedge dina6, posedge clka &&& blka_wen, 0.0, notify_reg);
      $setup(negedge dina6, posedge clka &&& blka_wen, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, posedge dina6, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, negedge dina6, 0.0, notify_reg);
      $setup(posedge dina5, posedge clka &&& blka_wen, 0.0, notify_reg);
      $setup(negedge dina5, posedge clka &&& blka_wen, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, posedge dina5, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, negedge dina5, 0.0, notify_reg);
      $setup(posedge dina4, posedge clka &&& blka_wen, 0.0, notify_reg);
      $setup(negedge dina4, posedge clka &&& blka_wen, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, posedge dina4, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, negedge dina4, 0.0, notify_reg);
      $setup(posedge dina3, posedge clka &&& blka_wen, 0.0, notify_reg);
      $setup(negedge dina3, posedge clka &&& blka_wen, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, posedge dina3, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, negedge dina3, 0.0, notify_reg);
      $setup(posedge dina2, posedge clka &&& blka_wen, 0.0, notify_reg);
      $setup(negedge dina2, posedge clka &&& blka_wen, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, posedge dina2, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, negedge dina2, 0.0, notify_reg);
      $setup(posedge dina1, posedge clka &&& blka_wen, 0.0, notify_reg);
      $setup(negedge dina1, posedge clka &&& blka_wen, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, posedge dina1, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, negedge dina1, 0.0, notify_reg);
      $setup(posedge dina0, posedge clka &&& blka_wen, 0.0, notify_reg);
      $setup(negedge dina0, posedge clka &&& blka_wen, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, posedge dina0, 0.0, notify_reg);
      $hold(posedge clka &&& blka_wen, negedge dina0, 0.0, notify_reg);


      $setup(posedge wena, posedge clka &&& reset, 0.0, notify_reg);
      $setup(negedge wena, posedge clka &&& reset, 0.0, notify_reg);
      $hold(posedge clka &&& reset, posedge wena, 0.0, notify_reg);
      $hold(posedge clka &&&reset, negedge wena, 0.0, notify_reg);

      $setup(posedge blka, posedge clka &&& reset, 0.0, notify_reg);
      $setup(negedge blka, posedge clka &&& reset, 0.0, notify_reg);
      $hold(posedge clka &&& reset, posedge blka, 0.0, notify_reg);
      $hold(posedge clka &&& reset, negedge blka, 0.0, notify_reg);

      $recovery(posedge reset, posedge clka, 0.0, notify_reg);
      $hold(posedge clka,posedge reset, 0.0, notify_reg);

      $recovery(posedge reset, posedge clkb, 0.0, notify_reg);
      $hold(posedge clkb,posedge reset, 0.0, notify_reg);

      $width(negedge reset, 0.0, 0, notify_reg);


      $setup(posedge addrb11, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $setup(negedge addrb11, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, posedge addrb11, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, negedge addrb11, 0.0, notify_reg);
      $setup(posedge addrb10, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $setup(negedge addrb10, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, posedge addrb10, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, negedge addrb10, 0.0, notify_reg);
      $setup(posedge addrb9, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $setup(negedge addrb9, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, posedge addrb9, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, negedge addrb9, 0.0, notify_reg);
      $setup(posedge addrb8, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $setup(negedge addrb8, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, posedge addrb8, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, negedge addrb8, 0.0, notify_reg);
      $setup(posedge addrb7, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $setup(negedge addrb7, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, posedge addrb7, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, negedge addrb7, 0.0, notify_reg);
      $setup(posedge addrb6, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $setup(negedge addrb6, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, posedge addrb6, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, negedge addrb6, 0.0, notify_reg);
      $setup(posedge addrb5, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $setup(negedge addrb5, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, posedge addrb5, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, negedge addrb5, 0.0, notify_reg);
      $setup(posedge addrb4, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $setup(negedge addrb4, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, posedge addrb4, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, negedge addrb4, 0.0, notify_reg);
      $setup(posedge addrb3, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $setup(negedge addrb3, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, posedge addrb3, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, negedge addrb3, 0.0, notify_reg);
      $setup(posedge addrb2, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $setup(negedge addrb2, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, posedge addrb2, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, negedge addrb2, 0.0, notify_reg);
      $setup(posedge addrb1, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $setup(negedge addrb1, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, posedge addrb1, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, negedge addrb1, 0.0, notify_reg);
      $setup(posedge addrb0, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $setup(negedge addrb0, posedge clkb &&& blkb_en, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, posedge addrb0, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_en, negedge addrb0, 0.0, notify_reg);

      $setup(posedge dinb8, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $setup(negedge dinb8, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, posedge dinb8, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, negedge dinb8, 0.0, notify_reg);
      $setup(posedge dinb7, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $setup(negedge dinb7, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, posedge dinb7, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, negedge dinb7, 0.0, notify_reg);
      $setup(posedge dinb6, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $setup(negedge dinb6, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, posedge dinb6, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, negedge dinb6, 0.0, notify_reg);
      $setup(posedge dinb5, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $setup(negedge dinb5, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, posedge dinb5, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, negedge dinb5, 0.0, notify_reg);
      $setup(posedge dinb4, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $setup(negedge dinb4, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, posedge dinb4, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, negedge dinb4, 0.0, notify_reg);
      $setup(posedge dinb3, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $setup(negedge dinb3, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, posedge dinb3, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, negedge dinb3, 0.0, notify_reg);
      $setup(posedge dinb2, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $setup(negedge dinb2, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, posedge dinb2, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, negedge dinb2, 0.0, notify_reg);
      $setup(posedge dinb1, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $setup(negedge dinb1, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, posedge dinb1, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, negedge dinb1, 0.0, notify_reg);
      $setup(posedge dinb0, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $setup(negedge dinb0, posedge clkb &&& blkb_wen, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, posedge dinb0, 0.0, notify_reg);
      $hold(posedge clkb &&& blkb_wen, negedge dinb0, 0.0, notify_reg);


      $setup(posedge wenb, posedge clkb &&& reset, 0.0, notify_reg);
      $setup(negedge wenb, posedge clkb &&& reset, 0.0, notify_reg);
      $hold(posedge clkb &&& reset, posedge wenb, 0.0, notify_reg);
      $hold(posedge clkb &&& reset, negedge wenb, 0.0, notify_reg);

      $setup(posedge blkb, posedge clkb &&& reset, 0.0, notify_reg);
      $setup(negedge blkb, posedge clkb &&& reset, 0.0, notify_reg);
      $hold(posedge clkb &&& reset, posedge blkb, 0.0, notify_reg);
      $hold(posedge clkb &&& reset, negedge blkb, 0.0, notify_reg);

   endspecify
 endmodule
