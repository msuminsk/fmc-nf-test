`ifndef __FIP_HARDWARE_MODEL_INCLUDED
 `define __FIP_HARDWARE_MODEL_INCLUDED

interface FIPBus;
   wire p;
   wire n;

   // fieldrive-side port
   modport fd
     (
      inout p,
      inout n
      );

   // agent-side port
   modport bus
     (
      inout p,
      inout n
      );
   
endinterface // FIPBus

interface FieldriveFPGA;
   logic fd_rxcdn;
   logic fd_rxd;
   logic fd_txer;
   logic fd_wdgn;
   logic fd_rstn;
   logic fd_txck;
   logic fd_txd;
   logic fd_txena;
   logic [1:0] speed;
   logic       ext_sync;
   
   // fieldrive side
   modport fd 
     (
      input  fd_rxcdn,
      input  fd_rxd,
      input  fd_txer,
      input  fd_wdgn,
      output fd_rstn,
      output fd_txck,
      output fd_txd,
      output fd_txena,
      input speed
      );

   // FPGA side
   modport fpga
     (
      output fd_rxcdn,
      output fd_rxd,
      output fd_txer,
      output fd_wdgn,
      input  fd_rstn,
      input  fd_txck,
      input  fd_txd,
      input  fd_txena,
      output speed,
      output ext_sync
      );
   
   
endinterface // FieldriveFPGA

interface FieldriveModel 
(
 FieldriveFPGA.fd fpga,
 FIPBus.fd fip
 );

   task setSpeed( int s );
     case(s)
       31250:
	 fpga.speed = 0;
       1000000:
	 fpga.speed = 1;
       2500000:
	 fpga.speed = 2;
     endcase // case (s)
   endtask // setSpeed

   
   reg tx_reg = 0;
   reg txen_reg = 0;

   initial begin
      fpga.fd_txer = 0;
      fpga.fd_wdgn = 1;
   end
      
   
   always@(fpga.fd_rstn)
     if(!fpga.fd_rstn)
       begin
	  tx_reg <= 0;
	  txen_reg <= 0;
	  fpga.fd_wdgn <= 1;
       end
   

   always@(posedge fpga.fd_txck)
     begin
	tx_reg <= fpga.fd_txd;
	txen_reg <= fpga.fd_txena;
     end


   // fixme: comment
   always@*
     if ((fip.p == 1'b1 && fip.n == 1'b0) || (fip.p == 1'b0 && fip.n == 1'b1))
       fpga.fd_rxcdn <= 0;
     else
       fpga.fd_rxcdn <= 1;

   reg bus_error = 0;
   

   reg force_txer = 0;
   
   always@*
     if(!force_txer)
       fpga.fd_txer <= ( fip.p === 1'bx || fip.n === 1'bx);
     else
       fpga.fd_txer <= 1;
   
   task forceTxError ( bit err );
      force_txer <= err;
   endtask // forceTxError

   task forceWatchdogError ();
      fpga.fd_wdgn <= 0;
      #1;
      
   endtask // forceWatchdogReset
   
   
   assign fpga.fd_rxd = !fpga.fd_rxcdn ? fip.p : 1'b0;
//   assign fpga.fd_txer = ;
   
   assign fip.p = (txen_reg ? tx_reg : 1'bz);
   assign fip.n = (txen_reg ? ~tx_reg : 1'bz);

  // assign fpga.fd_wdgn = 1'b1;

	   
endinterface // FieldriveModel


interface MasterfipMezzanine
  (
   FieldriveFPGA.fpga fpga,
   FIPBus.fd fip
   );

   FieldriveModel U_Fieldrive
     ( fpga, fip );
   
   initial begin
      fpga.ext_sync = 0;
   end
  
   task automatic setSpeed ( int bitrate );
      case (bitrate)
	31250: fpga.speed = 0;
	1000000: fpga.speed = 1;
	2500000: fpga.speed = 2;
      endcase // case (bitrate)
   endtask // setSpeed
   
 
   task automatic pulseTrigger();
      fpga.ext_sync <= 1;
      #1us;
      fpga.ext_sync <= 0;
      #1;
   endtask // pulseTrigger

 
   
   function automatic int getSpeed();
      case (fpga.speed)
	0: return 31250;
	1: return 1000000;
	2: return 2500000;
      endcase // case (bitrate)
   endfunction // getSpeed
   
				   
   
  
endinterface // MasterfipMezzanine


`endif

