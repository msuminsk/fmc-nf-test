`timescale 1ps/1ps

`include "fip_phy_model.svh"
`include "fip_frame.svh"
`include "fip_hardware_model.svh"

module main;

FIPBus fip_bus();
FieldriveFPGA fieldrive();
FieldriveModel fd_model(.fpga(fieldrive), .fip(fip_bus.fd));
FipPHY fip_master(.fip_p(fip_bus.bus.p), .fip_n(fip_bus.bus.n));

// 40 MHz clock generator
reg clk_40m_nf = 0;
time clk_period_40m = 25ns;
always #(clk_period_40m / 2) clk_40m_nf <= ~clk_40m_nf;

// 125 MHz diff clock generator
reg clk_125m_p = 0;
reg clk_125m_n = 1;
time clk_period_125m = 8ns;
always #(clk_period_125m / 2)
begin
    clk_125m_p <= ~clk_125m_p;
    clk_125m_n <= ~clk_125m_n;
end

// Deassert reset after 3 clock cycles
reg rst_n = 0;
initial begin
    repeat(3) @(posedge clk_40m_nf);
    #(clk_period_40m / 5) rst_n <= 1;
end

wire [15:0] dat_i;
wire [15:0] dat_o;
wire [9:0] adr;
wire stb, wclk, we, cyc, ack, wrst;

wire rstin, rston, var1_acc, var1_rdy, var2_acc, var2_rdy, var3_acc, var3_rdy;
wire [2:0] p3_lgth;  // Produced variable length
wire nostat;

reg [7:0] address = 8'h01;  // Substation data

nanofip DUT(
    .c_id_i    (4'hc),  // Constructor field
    .m_id_i    (4'ha),  // Model field
    .p3_lgth_i (p3_lgth),
    .rate_i    (fieldrive.speed),
    .subs_i    (address), // Station address
    .nostat_i  (nostat),

    .rstin_i  (rstin),
    .rston_o  (rston),  // Reset output
    .rstpon_i (rst_n),  // Power-on-reset
    .slone_i  (1'b0),   // Disable standalone mode
    .uclk_i   (clk_40m_nf),

    // Signals accessed by variables 1-3
    .var1_acc_i(var1_acc),
    .var2_acc_i(var2_acc),
    .var3_acc_i(var3_acc),

    .var1_rdy_o(var1_rdy),
    .var2_rdy_o(var2_rdy),
    .var3_rdy_o(var3_rdy),

    // Wishbone
    .dat_o (dat_i),
    .dat_i (dat_o),
    .adr_i (adr),
    .stb_i (stb),
    .wclk_i(wclk),
    .we_i  (we),
    .cyc_i (cyc),
    .ack_o (ack),
    .rst_i (wrst),

    // Receiver
    .fd_rxcdn_i(fieldrive.fd_rxcdn),
    .fd_rxd_i  (fieldrive.fd_rxd),
    .fd_txer_i (fieldrive.fd_txer),
    .fd_wdgn_i (fieldrive.fd_wdgn),

    // Transmitter
    .fd_rstn_o (fieldrive.fd_rstn),
    .fd_txck_o (fieldrive.fd_txck),
    .fd_txd_o  (fieldrive.fd_txd),
    .fd_txena_o(fieldrive.fd_txena),

    .r_fcser_o (),
    .r_tler_o  (),
    .u_cacer_o (),
    .u_pacer_o (),

    // JTAG interface
    .jc_tms_o(),
    .jc_tdi_o(),
    .jc_tck_o(),
    .jc_tdo_i(1'b0)
);

spec_top SPEC(
    .clk_125m_pllref_n_i(clk_125m_n),
    .clk_125m_pllref_p_i(clk_125m_p),

    .button1_i(1'b1),
    //.button2_i(1'b1),
    .led_red_o(),
    .led_green_o(),

    // Wishbone
    .dat_i(dat_i[7:0]),
    .dat_o(dat_o[7:0]),
    .adr_o(adr),
    .stb_o(stb),
    .wclk_o(wclk),
    .we_o(we),
    .cyc_o(cyc),
    .ack_i(ack),
    .rst_o(wrst),

    // FIP
    .rstin_o(rstin),
    //.rston_i(rston),
    .nostat_o(nostat),
    .var3_acc_o(var3_acc),
    .var3_rdy_i(var3_rdy),
    .var2_acc_o(var2_acc),
    .var2_rdy_i(var2_rdy),
    .var1_acc_o(var1_acc),
    .var1_rdy_i(var1_rdy),
    .p3_lgth_o(p3_lgth)
);

initial begin
    FipFrame fip_frame;

    fieldrive.speed = 'h01;    // 1Mbit
    fip_master.setSpeed(1000000);
    fip_master.setVerbose(1);

    #10us;      // wait for reset

    // presence test
    //fip_frame = FipFrame::makeMF_Presence(address);
    //fip_master.sendFrame(fip_frame);
    //#400us;     // wait for response

    // set var1
    fip_frame = FipFrame::makeMF_ID_DAT('h05, address);
    fip_master.sendFrame(fip_frame);
    #50us;      // silence time
    fip_frame = FipFrame::makeMF_RP_DAT('{'h11, 'h22, 'h33, 'h44, 'h55, 'h66, 'h77, 'h88});
    fip_master.sendFrame(fip_frame);        // TODO change to turnaround?
    #100us;       // wait for the consumed var to be copied to the production var

    // read var3, which should be equal to var1
    fip_frame = FipFrame::makeMF_ID_DAT('h06, address);
    fip_master.sendFrame(fip_frame);
    #1000us;

    // set var1
    fip_frame = FipFrame::makeMF_ID_DAT('h05, address);
    fip_master.sendFrame(fip_frame);
    #50us;      // silence time
    fip_frame = FipFrame::makeMF_RP_DAT('{'h99, 'haa, 'hbb, 'hcc, 'hdd, 'hee, 'hff, 'h55});
    fip_master.sendFrame(fip_frame);        // TODO change to turnaround?
    #100us;       // wait for the consumed var to be copied to the production var

    // read var3, which should be equal to var1
    fip_frame = FipFrame::makeMF_ID_DAT('h06, address);
    fip_master.sendFrame(fip_frame);
    #1000us;

    $stop();
end

endmodule // main
